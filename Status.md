# Phad Status
Status of this project & some development notes. See `Devlog.md` for historical notes

## BUGS
- `<willdelete><?phpecho "willdelete";exit;?></willdelete>` causes problems. Note the `<?phpecho` with no space before `echo`. It's probably okay if that gives an error, but the error is more subtle. The compiled output adds a question mark to the `?>` so it becomes `??>`... and I'm not sure if `<?phpecho` actually works as an open tag. I don't think so?

## Oct 24, 2023
Added custom/user-added attribute validators & recently added form validation documentation. Recently added p-type attribute and p-nohtml attribute for validation.

Added validation for radio input options, similar to the built-in select/option validation. 

## Dec 27, 2022
- NEED to update to scrawl v0.8. My `Examples.src.md` references methods that don't exist any more, presumably from old tests.

## Forms issue
What I want:
    - admin: view blank form, view previously submitted forms by id, UPDATE with id, INSERT with no id
    - anyone: view blank form, INSERT with no id. CANNOT update or view previously submitted forms 

Issues preventing this:
    - type='default' has a lot of magic
    - complexities between can_read_node, can_read_data, read_data, and possibly other data-related steps
        - i.e. data permissions & data loading seems to come from multiple places & it is very confusing

Potential solutions:
    - create a new type that satifies what I want (hard to implement bc issues above)
    - refactor phad to make it easier to write new implementations for data handling
    - write a custom access function that ... checks alla that (this could be part of Phad or just part of the site i'm building)

What I want to do to assess:
- review my data... access ... loading ... stuff & figure out what the architecture is / how it works.

FOR NOW, I'm just going to use a custom access function & leave this for some future time

## Undocumented CHANGES
- providing NO target on a form node will no longer redirect to `/`

## Undocumented HOW TO
- use `<p-data type="default" access="...">` to overwrite the automatic `"default"` node that gives access to everyone
    - Fixes issue where a public-submittable form should NOT be available for editing by the public
- use `<p-data type="black_hole">` to always return a black hole object


## Versions
- v0.1: (discontinued) Early development version using liaison v0.3, commonmark ^1.0, and tester v0.2,
- v0.2: CANCELED, in favor of v0.3's template design ... (old notes) Still early development, uses liaison v0.5, tester v0.3, commonmark ^2.0 (probably suggested, not required) ... (jan 7, 2022: still using commonmark 1.0 & may remove it ... idk)
- v0.3: uses liaison v0.5, tester v0.3, commonmark ^1.0, code scrawl v0.6 ... uses a new template design for compilation
- v0.4: Liaison v0.6
    - Commit `912d622` added breaking changes regarding `can_read_row()`. Commit `c3f423b` is the commit before that.
- v0.5: still Liaison v0.6. an attempt to refactor and clean up phad, but it went badly & I abandoned v0.5
- v0.6: still liaison v0.6. second attempt to refactor phad. Incomplete but may continue
- v0.7: liaison v0.6. Attempt to recreate from ground up, but of course using old code.
- v1.0: Liaison v0.6. Hopefully a successful refactor & stable, reasonable library.

## Feb 4, 2022 END OF Day
i cleaned up the entire repo except for `FAQ.md` ... readme has documentation. the todos and known issues are cleaned up & sorted. Deleted a bunch of old code. From my git log, it looks like i didn't write ANY code today ... how boring

## TODO (code)
- `<errors>` node gets auto-replaced with error output. Love it! Might like this to be default feature even without `<errors>` node present. Futher, would like this to ALSO display data-access errors that I typically use `<on s=###>` nodes for.
- fix `x-prop` not hiding itself!
- update documentation with the `Handler` interface & remove all the `access_handlers[]` from the server setup example code. Access handlers may need defining, but there aren't any built-in required ones in phad, far as i know
- DONE stop `<p-data>` from being loaded when a form is POSTed, bc the p-data query overwrites the `$_POST` data. 
- test `join` attribute for sql builder
- homogenize use of `can_access()`. I did this for `can_read_node` and `can_read_data`, but not yet for `can_delete` or `can_submit`. The ONLY place `parse_functions` should be called is from within `can_access()`
- add `item_from_file()` documentation
- normalize the candelete/diddelete/cansubmit setup code
- further normalize the `call()` ... so everything has access to print (even though that would be "false" and deny access)
- auto-add `<input type="hidden" name="id">` to forms
- add a way to disable the default data node
- enable sitemaps for static routes (perhaps a sitemap attribute on the route?)

## TODO (documentation)
- review FAQ.md
- make scrawl copy all files from .docsrc, not just the .src.md files
- document the default data node
- document the api (see line 31 of devlog.md for some help, maybe)
    - directly working with items
    - server setup
    - creating sitemap (and routing to it)
    - caching routes
    - liaison integration
- document what happens with `$_GET`, `$_POST`, `$args`, and `$ItemInfo->args` 
- check into claim "when trying to EDIT a form and an id is given, but there is no item found for that id, the form is not displayed"
- How does `<input name="title" prop="main_title">` work?

## Significant Known Issues & Security Issues
- the `default` data node may allow access to items that are disallowed by `<p-data>` nodes
- `<input type="backend" name="something">`: if `$_POST['something']` is set ... this could create problems if the code handling `something` fails to overwrite the user-submitted value.
    - Fix1: ALWAYS set `$ItemRow['something']` 
    - Fix2: `unset($ItemRow['something'])` at top of script

## Known Issues
- `<p-data type="default">` breaks forms that have other `<p-data>` nodes. DO NOT USE THE DEFAULT NODE along with other nodes in forms. I don't think this is a problem for non-form items.
- `WHERE (id = :id)` doesn't work because there MUST be a space or end of string after `:id`. Just add a space before the closing parenthesis
- form node might display multiple times ... idk why or how to recreate the problem
- (feb 3, 2022) all redirects send headers & exit if `$this->exit_on_redirect` is true (the default) ... it might be better to have a response object or something & add the headers to it. 
- (feb 1, 2022) After adding `ROUTE_META` and `SITEMAP_META` blocks to all compiled output, there's whitepsace after the SITEMAP_META block ... idk why ... it might be an issue at some point
- there is no built-in way to have multiple Phad directories ... two phad objects must be created to facilitate two different phad directories ... also requires a custom function to check between the multiple objects for a valid view
- when there is no html in a phad php file: `Uncaught TypeError: Argument 1 passed to Taeluf\PHTML::insertCodeBefore() must be an instance of DOMNode, null given` ... this should be handled and give a clear message
- there is no built-in way to add css & js files to a page ... i added `resource_files()` to the item object to get a list of files to make this easier.
- when a form fails to submit because there is no validation for the prop-type, there's no error reported ... it just fails silently ... I've placed an exception there, but what is really needed is to display the error on the form itself as it is returned to the user
- in a form, I have a nested item to display options (`<option item="Election" value="<?=$Election->id?>"><?=$Election->name?></option>`). This renders some code that has an automatic `selected` feature, but because the value is explicitly written in PHP, we compare against the election_id, but we don't actually compare against the real value. The compiled html is like `<option value="<?=$Election->id?>" <?=('<?=$Election->id?>'==$Office->election_id)? ' selected="" ' : ' '?>><?=$Election->name?></option>`. If I try wrapping it in an `<x-item>` & using a `prop=id` on the `<option>`, then the string-quoted php code is replaced by a `NULL` and the option text is auto-filled ...
- when a value is in `$_GET[]` adding it to the `$args` or to the item is kind of cumbersome


## Things I want (but may not do)
- enable sitemap handlers
- Using filters for custom submission
    - Ex: Form has `<input type="text" prop="title">` & wants slug. Add `<input type="computed" from="title" filter="slugify" to="slug">` 
- modals to edit forms, especially as a result of `api="Blog:create"` clicks
    - For further refinement: `api="Card.Full:view" api-on="click, touch" `api-style="dialog | page" ... also allow `api2="whatever:create"` ...
    - Or: `api="onclick:Blog:create"` idk ...
    - automatic routes for every api??
- integration with schema.org (idk what i mean, really)
- javascript for handling modals, submissions ... idk
- apis based around `Blog:create`, like getting the route, or submitting data, or automatically linking an html node to that page
- set `$this->info = $ItemInfo`, so the item object can be accessed after ANY operations
- rename `cols` attribute to `select` (since the others are verb-based)
- log all errors to a file
- deprecate `submit_errors` in favor of `errors` ... maybe
- automatic pagination ...
- file download routes ... (or did i add those?)
- Default prop values in forms and items with `default="some value"`
- use `accepts="image/png"` to limit the files that can be uploaded
- automatically setup `file` inputs with the appropriate backend-properties (rn its totally manual)

## Cool ideas i may not care about
- have an item node be a prop of a parent item node, auto-enabling the relational query
    - `JOIN`s: some other way to unite `Person` and `BlogPosts` across `person_blogs` m2m table
- a simpler setup using a config file??
- multi-page forms ... no idea how i would implement this
- configurable node names (like use `<data>` instead of `<p-data>`)
- extensibility in compilation ... no idea what this would look like (onCompile hook?)
- treat a `<select>` as an item & fill in it's `<option>`s from a query (i think this is possible rn, but idk ... it would probably fail validation)
- create a synchronization addon ... to sync a view with an item in the db (or a list)
- global error handlers like `on404` or ... something ...
- add auto-handling for `<img>` tags to add src & alt
- add auto-handling for `<a>` tags to add href & inner text
- `<sitemap>`
    - `<sitemap name="BlogPosts">` to explicitly name the sitemap file & route.
    - Output multiple sitemap files & an index sitemap file listing all of them
- `<p-data>` error and pass/fail attributes ... attributes for error handling or pre-emptive failure handling like `log_error="always"` or `stop_if_fail="true"` to stop the next `<p-data>` from running (and a similar for `skip_if_pass`)
- `SELECT` queries: list columns explicitly by scanning `properties` 
- js & modal stuff (see `.docsrc/History/Modal/*`)
