This page only for viewing draft blog posts
<div item="Blog">
    <p-data name="draft" 
        where="Blog.slug LIKE :slug AND Blog.status LIKE 'draft'" limit="0,5" 
        access="role:admin|moderator;call:CanViewDraftPost;"
    ></p-data>
    <h1 prop="title"></h1>
    <p prop="description"></p>
</div>
