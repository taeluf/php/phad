<route pattern="/blog/{slug}/"> 
    <sitemap 
        sql="SELECT slug, last_mod AS sm_lastmod FROM blog" 
        handler="namespace:blogSitemapHandler"
        priority=0.8 
        changefreq=weekly
    >
    </sitemap>
</route>

<div item="Blog">
    <p-data where="Blog.slug LIKE :slug"></p-data>
    <h1 prop="title"></h1>
    <p prop="description"></p>
</div>

