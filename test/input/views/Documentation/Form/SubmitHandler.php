<form item="Blog" target="/blog/{slug}/">
    Since `slug` is not (& shouldn't be) part of the form, we have to compute it on submit & use that for our redirection target.
    <onsubmit><?php
        $BlogRow['slug'] = str_replace(' ', '-', strtolower($BlogRow['title']));
        /** set phad_mode to anything other than `submit` to skip submission (form will be displayed). */
        if (false)$BlogItem->mode = 'cancel_submit';
    ?>
        On submission failure, the form will still be displayed. And any non-php inside the `onsubmit` tag will also be displayed (above the form tag). Though the onsubmit tag itself will be removed.
    </onsubmit>
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>


    This `backend` node will be removed. But its required, so Phad knows to accept the `slug` attribute
    <input type="backend" name="slug" minlength=4 maxlength=150 />
</form>
