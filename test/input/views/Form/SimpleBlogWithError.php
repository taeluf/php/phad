<form item="Blog" target="/blog/{title}/">
    <div class="my-error-class" loop="inner" item="BlogSubmitErrors">
        <p prop="msg"></p>
    </div>
    <input type="text" name="title" maxlength="75" required>
    <textarea name="body" maxlength="2000" minlength="50"></textarea>
</form>
