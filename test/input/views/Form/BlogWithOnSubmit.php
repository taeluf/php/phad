<form item="Blog">
    <onsubmit><?php
        $BlogRow['slug'] = str_replace(' ', '-', strtolower($BlogRow['title']));
        /** set phad_mode to anything other than `submit` to skip submission (form will be displayed). */
        if (false)$BlogItem->phad_mode = 'cancel_submit';
    ?></onsubmit>
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>

    <input type="backend" name="slug" minlength=4 maxlength=150 />
</form>
