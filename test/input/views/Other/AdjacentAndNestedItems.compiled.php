<?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
);
}
?><?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$PageInfo = (object)[
    'name'=> 'Page', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($PageInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $PageInfo;
}

foreach ($PageInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $PageInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $PageInfo)){
        $PageInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $PageInfo->rows = $phad->read_data($_node, $PageInfo);
    if (count($PageInfo->rows)==0){
        $PageInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $PageInfo->response_code = 200;
    $_node['response_code'] = 200;
    $PageInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($PageInfo);
}




if (count($PageInfo->rows)===0){
    $phad->no_rows_loaded($PageInfo);
}

foreach ($PageInfo->rows as $RowIndex_Page => $PageRow ): 
    $Page = $phad->object_from_row($PageRow, $PageInfo);
    

    
    ?><div>
    <h1><?=$Page->title?></h1>
    <?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$BlogInfo = (object)[
    'name'=> 'Blog', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($BlogInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $BlogInfo;
}

foreach ($BlogInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $BlogInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $BlogInfo)){
        $BlogInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $BlogInfo->rows = $phad->read_data($_node, $BlogInfo);
    if (count($BlogInfo->rows)==0){
        $BlogInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $BlogInfo->response_code = 200;
    $_node['response_code'] = 200;
    $BlogInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($BlogInfo);
}




if (count($BlogInfo->rows)===0){
    $phad->no_rows_loaded($BlogInfo);
}

foreach ($BlogInfo->rows as $RowIndex_Blog => $BlogRow ): 
    $Blog = $phad->object_from_row($BlogRow, $BlogInfo);
    

    
    ?><div>
        <h1><?=$Blog->title?></h1>
        <p><?=$Blog->description?></p>
        <?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$WriterInfo = (object)[
    'name'=> 'Writer', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($WriterInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $WriterInfo;
}

foreach ($WriterInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $WriterInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $WriterInfo)){
        $WriterInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $WriterInfo->rows = $phad->read_data($_node, $WriterInfo);
    if (count($WriterInfo->rows)==0){
        $WriterInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $WriterInfo->response_code = 200;
    $_node['response_code'] = 200;
    $WriterInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($WriterInfo);
}




if (count($WriterInfo->rows)===0){
    $phad->no_rows_loaded($WriterInfo);
}

foreach ($WriterInfo->rows as $RowIndex_Writer => $WriterRow ): 
    $Writer = $phad->object_from_row($WriterRow, $WriterInfo);
    

    
    ?><div>
            <h1><?=$Writer->name?></h1>
        </div><?php
endforeach;

?>

    </div><?php
endforeach;

?>

    <?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$SongInfo = (object)[
    'name'=> 'Song', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($SongInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $SongInfo;
}

foreach ($SongInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $SongInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $SongInfo)){
        $SongInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $SongInfo->rows = $phad->read_data($_node, $SongInfo);
    if (count($SongInfo->rows)==0){
        $SongInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $SongInfo->response_code = 200;
    $_node['response_code'] = 200;
    $SongInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($SongInfo);
}




if (count($SongInfo->rows)===0){
    $phad->no_rows_loaded($SongInfo);
}

foreach ($SongInfo->rows as $RowIndex_Song => $SongRow ): 
    $Song = $phad->object_from_row($SongRow, $SongInfo);
    

    
    ?><div>
        <h1><?=$Song->title?></h1>
        <p><?=$Song->description?></p>
        <?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$ArtistInfo = (object)[
    'name'=> 'Artist', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($ArtistInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $ArtistInfo;
}

foreach ($ArtistInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $ArtistInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $ArtistInfo)){
        $ArtistInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $ArtistInfo->rows = $phad->read_data($_node, $ArtistInfo);
    if (count($ArtistInfo->rows)==0){
        $ArtistInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $ArtistInfo->response_code = 200;
    $_node['response_code'] = 200;
    $ArtistInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($ArtistInfo);
}




if (count($ArtistInfo->rows)===0){
    $phad->no_rows_loaded($ArtistInfo);
}

foreach ($ArtistInfo->rows as $RowIndex_Artist => $ArtistRow ): 
    $Artist = $phad->object_from_row($ArtistRow, $ArtistInfo);
    

    
    ?><div>
            <h1><?=$Artist->name?></h1>
        </div><?php
endforeach;

?>

    </div><?php
endforeach;

?>

</div><?php
endforeach;

?>

<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$FooterInfo = (object)[
    'name'=> 'Footer', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
    
    
];



$phad->item_initialized($FooterInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $FooterInfo;
}

foreach ($FooterInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $FooterInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $FooterInfo)){
        $FooterInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $FooterInfo->rows = $phad->read_data($_node, $FooterInfo);
    if (count($FooterInfo->rows)==0){
        $FooterInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $FooterInfo->response_code = 200;
    $_node['response_code'] = 200;
    $FooterInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($FooterInfo);
}




if (count($FooterInfo->rows)===0){
    $phad->no_rows_loaded($FooterInfo);
}

foreach ($FooterInfo->rows as $RowIndex_Footer => $FooterRow ): 
    $Footer = $phad->object_from_row($FooterRow, $FooterInfo);
    

    
    ?><div>
    <p><?=$Footer->copyright?></p>
</div><?php
endforeach;

?>

