<route pattern="/blog/{slug}"> 
    <sitemap 
        sql="SELECT slug, last_mod, search_priority/100.00 AS priority FROM blog" 
        handler="namespace:blogSitemapHandler"
        priority=0.8 
    >
    </sitemap>
</route>
