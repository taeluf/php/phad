<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$BlogInfo = (object)[
    'name'=> 'Blog', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'view',
  1 => 'data',
), //rawdata generally json. could be sql or csv though
    'type'=>'view', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'name' => 'type',
    'where' => 'Blog.type LIKE :type',
    'limit' => '0,2',
    'type' => 'node',
  ),
  1 => 
  array (
    'name' => 'nottype',
    'where' => 'Blog.type NOT LIKE :type',
    'limit' => '0,3',
    'type' => 'node',
  ),
  2 => 
  array (
    'name' => 'whatever',
    'where' => 'Blog.type NOT LIKE whatever',
    'limit' => '0,3',
    'type' => 'node',
  ),
  3 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    
    
];



$phad->item_initialized($BlogInfo);





if ($phad_block==\Phad\Blocks::ITEM_META){
    return $BlogInfo;
}

foreach ($BlogInfo->data_nodes as $_index=>$_node){
    if (!$phad->can_read_data($_node, $BlogInfo)){
        $BlogInfo->response_code = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $BlogInfo->rows = $phad->read_data($_node, $BlogInfo);
    if (count($BlogInfo->rows)==0){
        $BlogInfo->response_code = 404;
        continue;
    }
    $BlogInfo->response_code = 200;
    $BlogInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($BlogInfo);
}


if ($BlogInfo->response_code == 200){
    // for this, i could use a helper function that builds an array of ifs
    // and then print it here where the ifs are the data index == whatever
    // so the code_for_response_200 is programattically generated and then inserted into this template

        if ($BlogInfo->data_index === 0 ):
        ?><h1>Blogs with type: <?=$BlogInfo->args['type']?></h1><?php
    endif;
    if ($BlogInfo->data_index === 2 ):
        ?><h1>Whatever Blogs</h1><?php
    endif;
    if ($BlogInfo->data_index === 3 ):
        ?><p>Type: <?=$args['type']?></p><?php
    endif;

}



elseif ($BlogInfo->response_code == 404){
        if ($BlogInfo->data_index === 1 ):
        ?><p>There were none found for type not like <?=$type?></p><?php
    endif;
    if ($BlogInfo->data_index === 3 ):
        ?><p>There were no blogs found...</p><?php
    endif;

}


// i can use @else2 to include an else statement here

foreach ($BlogInfo->rows as $RowIndex_Blog => $BlogRow ): 
    $Blog = $phad->object_from_row($BlogRow, $BlogInfo);
    

    
    ?><div>
    
    
    
    
    
    <h1><?=$Blog->title?></h1>
    <p><?=$Blog->description?></p>
</div><?php
endforeach;

?>

