<form item="Blog" target="/blog/{title}/">
    <input type="text" name="title" maxlength="75">
    <textarea name="body" maxlength="2000" minlength="50"></textarea>
    <select name="category">
        <option>Select One</option>
        <option value="social-justice">Social Justice</option>
        <option value="policy">Policy</option>
        <option value="Police Brutality">Police Brutality</option>
        <option value="Election">Election</option>
    </select>
</form>
