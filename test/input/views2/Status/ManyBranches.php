<div item="Blog">
    <p-data name="type" where="Blog.type LIKE :type" limit="0,2">
        <on s=200><h1>Blogs with type: <?=$Blog->type?></h1></on>
    </p-data>
    <p-data name="nottype" where="Blog.type NOT LIKE :type" limit="0,3">
        <on s=404><p>There were none found for type not like <?=$type?></p></on>
    </p-data>
    <p-data name="whatever" where="Blog.type NOT LIKE whatever" limit="0,3">
        <on s=200><h1>Whatever Blogs</h1></on>
    </p-data>
    <on s=404><p>There were no blogs found...</p></on>
    <on s=200><p>Type: <span prop="type"></span></p></on>
    <h1 prop="title"></h1>
    <p prop="description"></p>
</div>
