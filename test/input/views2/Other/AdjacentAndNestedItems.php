<div item="Page">
    <h1 prop="title"></h1>
    <div item="Blog">
        <h1 prop="title"></h1>
        <p prop="description"></p>
        <div item="Writer">
            <h1 prop="name"></h1>
        </div>
    </div>
    <div item="Song">
        <h1 prop="title"></h1>
        <p prop="description"></p>
        <div item="Artist">
            <h1 prop="name"></h1>
        </div>
    </div>
</div>
<div item="Footer">
    <p prop="copyright"></p>
</div>
