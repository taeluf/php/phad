<?php

namespace Phad\Test\Compilation;

class Exec extends \Phad\Tester {

    /** compiles the view, passes args to it, and compares against expected html
     * @test final otuput after compilation and `eval()`
     */
    public function run_exec_test($view, $args, $expect, $handlers=[]){

        $c2 = new \Phad\TemplateCompiler();
        $compiled = $c2->compile($view, file_get_contents($this->file('code/template/main.php')) );

        $phad_block = $args['phad_block'] ?? \Phad\Blocks::VIEW;
        $phad = new \Phad();
        $phad->handlers = array_merge($phad->handlers, $handlers);

        // echo $compiled;
        // exit;
        ob_start();
        eval('?>'.$compiled);
        $actual = ob_get_clean();


        // print_r($args);
        // echo "\n\n\n-----------\n\n";
        // echo $actual;
        // exit;
        $this->compare_lines($expect, $actual);
    }

    public function testCanReadNode2(){
        $view = <<<HTML
            <a href="/whatever/" access="role:guest">Am Link</a>
        HTML;
        $expect = <<<HTML
            <a href="/whatever/">Am Link</a>
        HTML;

        $this->run_exec_test($view, [], $expect,
            ['user_has_role'=>function(){return true;}]
        );
    }

    /**
     * @feature(Show submission errors in a form) Using a node like `<div class="errors" item="BlogSubmitErrors" loop="inner">`
     * @test showing submission errors in a form item
     */
    public function testFormWithErrorItem(){
        $view = <<<HTML
            <form item="Blog">
                <errors></errors>
                <input type="text" name="title" minlength=20 required>
            </form>
        HTML;
        $expect = <<<HTML
            <form action="" method="POST">
                <div class="errors">
                    <p>'title' failed validation for 'type:text'</p>
                    <p>'title' failed validation for 'minlength:20'</p>
                    <p>'title' failed validation for 'required:true'</p>
                </div>
                <input type="text" name="title" minlength="20" required value="">
            </form>
        HTML;

        $args = [
            'phad_block' => \Phad\Blocks::FORM_SUBMIT,
            'Blog'=>[
                'title'=>null,
            ],
        ];

        $this->run_exec_test($view, $args, $expect);

    }

    public function testInnerLoop(){

        $view = <<<HTML
            <div item="Blog" loop="inner">
                <h1 prop="title"></h1>
            </div>
        HTML;
        $expect = 
            <<<HTML
                <div>
                    <h1>Good Title!</h1>
                </div>
            HTML;

        $args = ['Blog'=>['title'=>'Good Title!']];

        $this->run_exec_test($view, $args, $expect);

    }

    /**
     * @test taking `BlogList=[[blog1], [blog2], [blog3]]` as data
     */
    public function testBlogListArg(){
        $view = <<<HTML
            <div item="Blog">
                <h1 prop="title"></h1>
                <p prop="intro"></p>
            </div>
        HTML;

        $args = [
            'BlogList'=>[
                ['title'=>'A Title', 'intro'=> "Intro paragraph"],
                ['title'=>'A Title 2', 'intro'=> "Second paragraph"],
            ]
        ];

        $expect = <<<HTML
            <div>
                <h1>A Title</h1>
                <p>Intro paragraph</p>
            </div><div>
                <h1>A Title 2</h1>
                <p>Second paragraph</p>
            </div>
        HTML;

        $this->run_exec_test($view, $args, $expect);
    
    }

    /**
     * @test taking `['Blog'=>['title'=>'somethin', ...]]` as data
     */
    public function testBlogArg(){
        $view = <<<HTML
            <div item="Blog">
                <h1 prop="title"></h1>
                <p prop="intro"></p>
            </div>
        HTML;

        $args = [
            'Blog'=>['title'=>'A Title', 'intro'=> "Intro paragraph"],
        ];

        $expect = <<<HTML
            <div>
                <h1>A Title</h1>
                <p>Intro paragraph</p>
            </div>
        HTML;

        $this->run_exec_test($view, $args, $expect);
    }

}
