<?php

namespace Phad\Test\Unit;

class Sitemap extends \Phad\Tester {

// - build a sitemap:
//     - take in a pattern, sql, and other sitemap attributes
//     - add custom handlers to modify sitemap data on a row-by-row basis
//     - generate sitemap xml
//     - write xml to disk

    public function testQueryToXml(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['slug'=>'varchar(50)']);
        $ldb->insert('blog', ['slug'=>'cat']);
        $ldb->insert('blog', ['slug'=>'dog']);
        $ldb->insert('blog', ['slug'=>'bear']);
        $sitemap_data =  
        [
            'sql' => 'SELECT slug FROM blog',
            'priority' => '0.8',
            'last_mod' => '1354',
            'changefreq' => 'daily',
            'pattern' => '/blog/{slug}',
        ];

        $dir = $this->file('test/input/sitemap');
        $this->empty_dir($dir);
        $sb = new \Phad\SitemapBuilder($dir);
        
        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $sb->router = $lia->addon('lia:server.router');
        $sb->pdo = $ldb->pdo();

        $entries = $sb->build_entries($sitemap_data);

        foreach($entries as $e){
            $sb->addEntry('query-to-xml', $e);
        }
        $sb->close('query-to-xml');

        $saved_entries = $sb->get_stored_entries('query-to-xml');


        $base = ['priority'=>'0.8', 'lastmod'=>'1354', 'changefreq'=>'daily'];
        $cat = array_merge($base, ['loc'=>'/blog/cat']);
        $dog = array_merge($base, ['loc'=>'/blog/dog']);
        $bear = array_merge($base, ['loc'=>'/blog/bear']);

        $this->compare_arrays(
            [$cat, $dog, $bear],
            $saved_entries
        );
    }

    public function testFillPattern(){
        $dir = $this->file('/test/input/sitemap/');
        $sb = new \Phad\SitemapBuilder($dir);
        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $sb->router = $lia->addon('lia:server.router');


        $pattern = '/blog/{slug}/{title}/';
        $parsed = $sb->parse_pattern($pattern);
        $filled = $sb->fill_pattern($parsed, ['slug'=>'cat','title'=>'all-about-my-cat']);

        $this->compare(
            '/blog/cat/all-about-my-cat/',
            $filled,
        );
    }


    /**
     * @test that results from the query will override default sitemap-params like priority, changefreq, and last_mod,
     */
    public function testQueryForSitemapParams(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['slug'=>'varchar(50)', 'lastmod'=>'varchar(2)', 'priority'=>'varchar(15)', 'changefreq'=>'varchar(15)']);
        $ldb->insert('blog', ['slug'=>'cat', 'lastmod'=>'2', 'priority'=>'0.5']);
        $ldb->insert('blog', ['slug'=>'dog', 'lastmod'=>'2', 'changefreq'=>'weekly']);
        $ldb->insert('blog', ['slug'=>'bear', 'lastmod'=>'3']);
        $dir = $this->file('test/input/sitemap');
        $sitemap_data =  
        [
            'sql' => 'SELECT slug, lastmod as last_mod, priority, changefreq FROM blog',
            'priority' => '0.8',
            'last_mod' => '1354',
            'changefreq' => 'daily',
            'pattern' => '/blog/{slug}',
        ];

        $sb = new \Phad\SitemapBuilder($dir);
        
        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $sb->router = $lia->addon('lia:server.router');
        $sb->pdo = $ldb->pdo();

        $entries = $sb->build_entries($sitemap_data);

        $base = ['priority'=>'0.8', 'last_mod'=>'2', 'changefreq'=>'daily'];
        $cat = array_merge($base, ['loc'=>'/blog/cat', 'priority'=>'0.5']);
        $dog = array_merge($base, ['loc'=>'/blog/dog', 'changefreq'=>'weekly']);
        $bear = array_merge($base, ['loc'=>'/blog/bear', 'last_mod'=>'3']);

        $this->compare_arrays(
            [$cat, $dog, $bear],
            $entries
        );
    }


    public function testNoQuery(){
        $sitemap_data =  
        [
            'priority' => '0.8',
            'last_mod' => '1354',
            'changefreq' => 'daily',
            'pattern' => '/blog/', //such as an index page 
        ];

        $dir = $this->file('test/input/sitemap');
        $sb = new \Phad\SitemapBuilder($dir);

        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $sb->router = $server->addons['router'];


        $entries = $sb->build_entries($sitemap_data);

        $target = 
        [
            'priority'=>'0.8', 
            'last_mod'=>'1354',
            'changefreq'=>'daily',
            'loc'=>'/blog/',
        ];

        $this->compare_arrays(
            [$target],
            $entries
        );
    }

    public function testGetEntries(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['slug'=>'varchar(50)']);
        $ldb->insert('blog', ['slug'=>'cat']);
        $ldb->insert('blog', ['slug'=>'dog']);
        $ldb->insert('blog', ['slug'=>'bear']);
        $dir = $this->file('test/input/sitemap');
        $sitemap_data =  
        [
            'sql' => 'SELECT slug FROM blog',
            'priority' => '0.8',
            'last_mod' => '1354',
            'changefreq' => 'daily',
            'pattern' => '/blog/{slug}',
        ];

        $sb = new \Phad\SitemapBuilder($dir);
        
        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $sb->router = $lia->addon('lia:server.router');
        $sb->pdo = $ldb->pdo();

        $entries = $sb->build_entries($sitemap_data);

        $base = ['priority'=>'0.8', 'last_mod'=>'1354', 'changefreq'=>'daily'];
        $cat = array_merge($base, ['loc'=>'/blog/cat']);
        $dog = array_merge($base, ['loc'=>'/blog/dog']);
        $bear = array_merge($base, ['loc'=>'/blog/bear']);

        $this->compare_arrays(
            [$cat, $dog, $bear],
            $entries
        );
    }

    
    /**
     * @test that `</urlset>` gets written during __destruct()
     */
    public function testSimpleWriting2(){
        $dir = $this->file('test/input/sitemap');
        $this->empty_dir($dir);
        $sb = new \Phad\SitemapBuilder($dir);
        $sb->addEntry('main2',
            $entry=['priority'=>'0.8', 'lastmod'=>''.microtime(true), 'changefreq'=>'daily']
        );

        // unset($sb);
        $sb2 = new \Phad\SitemapBuilder($dir);


        $data = $sb2->get_stored_entries('main2');

        $this->compare_arrays(
            $entry,
            $data[0]
        );

        $this->test('sitemap not yet closed');
        $sm_file = file_get_contents($this->file('test/input/sitemap/main2.xml'));
        $this->str_not_contains($sm_file,
            '</urlset>'
        );


        $this->test('sitemap closed after destroying SitemapBuilder');
        unset($sb);
        $sm_file = file_get_contents($this->file('test/input/sitemap/main2.xml'));

        $this->str_contains($sm_file,
            '</urlset>'
        );


    }

    /**
     * @test that writing entries works
     */
    public function testSimpleWriting(){
        $dir = $this->file('test/input/sitemap');
        $this->empty_dir($dir);
        $sb = new \Phad\SitemapBuilder($dir);
        $sb->addEntry('main',
            $entry=['priority'=>'0.8', 'lastmod'=>''.microtime(true), 'changefreq'=>'daily']
        );

        $sb->close('main');


        $data = $sb->get_stored_entries('main');

        // ksort($entry);
        // ksort($data[0]);
        $this->compare_arrays(
            $entry,
            $data[0]
        );
    }



    /**
     * @test running query
     */
    public function testGetResults(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['slug'=>'varchar(50)']);
        $ldb->insert('blog', ['slug'=>'cat']);
        $ldb->insert('blog', ['slug'=>'dog']);
        $ldb->insert('blog', ['slug'=>'bear']);
        $dir = $this->file('test/input/sitemap');

        $sb = new \Phad\SitemapBuilder($dir);
        $sb->pdo = $ldb->pdo();

        $results = $sb->get_results('SELECT slug FROM blog');

        $this->compare(
            [['slug'=>'cat'],['slug'=>'dog'],['slug'=>'bear']],
            $results
        );
    }

}
