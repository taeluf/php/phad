<?php

namespace Phad\Test\Unit;

/**
 * @test validation of an array of values based upon a property schema array
 * @test validation of individual properties (no property schema array)
 *
 */
class Item extends \Phad\Tester {

    public function testItemFromFile(){
        $phad = new \Phad();
        $phad->force_compile = true;
        $item = $phad->item_from_file($this->file('test/input/standalone/SomeItem.php'));

        $this->compare_lines(
            '<div>
            <h1>No Items</h1>
            </div>',
            $item.''
        );

        $this->is_file($this->file('test/input/standalone/SomeItem.compiled.php'));

    }

    public function testResourceFiles(){
        $prefix = $this->file('test/input/views3');
        $view = new \Phad\Item('List', $this->file('test/input/views3'),[]);
        $files = $view->resource_files();
        print_r($files);

        $remove_path = function($path) use ($prefix){
            return substr($path,strlen($prefix)+1);
        };

        $js = array_map($remove_path, $files['js']);
        $css = array_map($remove_path, $files['css']);

        $this->compare(
            ['List.css', 'List/one.css', 'List/two.css'],
            $css
        );
        $this->compare(
            ['List.js', 'List/one.js', 'List/two.js'],
            $js
        );
    }
}
