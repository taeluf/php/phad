<?php

namespace Phad\Test\Unit;

class PDOSubmitter extends \Phad\Tester {

    public function testUpdate(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['id'=>'integer PRIMARY KEY AUTOINCREMENT','title'=>'varchar(80)', 'body'=>'varchar(80)', 'rank'=>'integer']);
        $pdo = $ldb->pdo();
        $submitter = new \Phad\PDOSubmitter($pdo);

        $new_entry = 
        [
            'id'=>null,
            'title'=>'abc',
            'body'=>'this is text',
            'rank'=>37
        ];

        $ldb->insert('blog',['title'=>'afd']); // just to throw in extra data
        $new_entry['id'] = $ldb->insert('blog', $new_entry);

        $new_entry['rank'] = 60;
        $submitter->submit(
            'blog', $new_entry
        );


        $this->compare(
            [$new_entry],
            $ldb->select('blog', [':title'=>'abc'])
        );
    }


    public function testInsert(){
        $ldb = \Tlf\LilDb::sqlite();
        $ldb->create('blog', ['title'=>'varchar(80)', 'body'=>'varchar(80)', 'rank'=>'integer']);
        $pdo = $ldb->pdo();
        $submitter = new \Phad\PDOSubmitter($pdo);

        $new_entry = 
        [
            'title'=>'abc',
            'body'=>'this is text',
            'rank'=>37
        ];
        $submitter->submit(
            'blog', $new_entry
        );


        $this->compare(
            [$new_entry],
            $ldb->query(
                'SELECT title, body, rank FROM blog WHERE title LIKE :title',
                [':title'=>'abc']
            )
        );
    }
}
