<?php

namespace Phad\Test\Unit;

class PropertyFilters extends \Phad\Tester {

    /**
     * @test adding a filter by name & then executing it
     */
    public function testFilter(){

        $filterer = new \Phad();
        $filterer->filters['phad:test_to_lower'] = 'strtolower';

        $in = 'I AM STRING';
        $target = 'i am string';
        $actual = $filterer->filter('phad:test_to_lower', $in);

        $this->compare($target, $actual);
    }

}
