<?php

namespace Phad\Test\Main;


/**
 * Feature tests with as few layers of integration as possible.
 * All of these tests should create a compiler, compile from string, and eval the compiled output.
 */
class Features extends \Phad\Tester {

    protected $pdo;
    /** the template file contents */
    protected $template;

    public function prepare(){
        $this->pdo = new \PDO('sqlite::memory:');
        $this->template = file_get_contents($this->file('code/template/main.php'));
    }

    public function testSimpleQueryOnAccessNode(){
        $this->makeBlogTable($ListOfBlogs);
        $pdo = $this->pdo;
        
        $view = <<<VIEW
            <div item="Blog">
                <p-data name="type" where="Blog.type LIKE :type" limit="0,2"></p-data>
                <h1 prop="title"></h1>
                <p prop="description"></p>
            </div>
        VIEW;
        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;
        // echo $view;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;
        $phad->pdo = $pdo;
        // $lia->set('phad:pdo', $pdo);
        
        $args = ['type'=>'pet'];


        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        echo $viewEvaled;

        $this->str_contains($viewEvaled, '<div>');
        $this->str_contains($viewEvaled, '</div>');
        $this->str_not_contains($viewEvaled, ['item=','prop=']);
        foreach ($ListOfBlogs as $Blog){
            $Blog = (object)$Blog;
            if ($Blog->type!='pet')continue;
            $this->str_contains($viewEvaled, '<h1>'.$Blog->title.'</h1>');
            $this->str_contains($viewEvaled, '<p>'.$Blog->description.'</p>');
        }
    }
    public function testNodeWithObjectList(){
        $this->itemList($BlogList);
        $view = $this->viewSrc('Other/NoAccess');

        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;
        echo $view;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;
        $args = ['BlogList'=>$BlogList];
        // print_r($args);
        // exit;

        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        $this->str_contains($viewEvaled, '<div>');
        $this->str_contains($viewEvaled, '</div>');
        $this->str_not_contains($viewEvaled, ['item=','prop=']);
        foreach ($BlogList as $Blog){
            $this->str_contains($viewEvaled, '<h1>'.$Blog['title'].'</h1>');
            $this->str_contains($viewEvaled, '<p>'.$Blog['description'].'</p>');
        }
    }

    public function testFormWithSelectOptionSetFromObject(){
        $Blog = [
            'title'=>'Pets',
            'choice'=>'cat'
        ];
        $view = <<<VIEW
            <form item="Blog">
                <h1>What kind of pet do you have?</h1>
                <input type="text" name="title">
                <select name="choice">
                    <option>Select One</option>
                    <option value="cat">Kitty Cat</option>
                    <option value="dog">Pup Doog</option>
                    <option value="bear">Big Ole Bear</option>
                </select>
            </form>
        VIEW;

        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;
        echo $view;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;

        $args = ['Blog'=>$Blog];

        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        $this->str_contains($viewEvaled, ['<form','</form>']);
        $this->str_not_contains($viewEvaled, ['item="Blog"']);
        $this->test('input')->str_contains($viewEvaled, ['<input', 'name="title"', 'value="'.$Blog->title.'"']);
        $this->test('options')->str_contains($viewEvaled, "selected=\"\" >Kitty Cat</option>");

        // echo $viewEvaled;
    
    }
    public function testFormWithObject(){
        $Blog = [
            'title'=>'Kids in cages is... bad',
            'description'=>'It was bad when Obama did it, when Trump did it, and now that Biden is doing it.'
        ];
        $view = <<<VIEW
            <form item="Blog">
                <input type="text" name="title">
                <textarea name="description">
            </form>
        VIEW;
        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;

        $args = ['Blog'=>$Blog];

        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        $this->str_contains($viewEvaled, ['<form','</form>']);
        $this->str_not_contains($viewEvaled, ['item="Blog"']);
        $this->test('input')->str_contains($viewEvaled, ['<input', 'name="title"', 'value="'.$Blog->title.'"']);
        $this->test('textarea')->str_contains($viewEvaled, ['<textarea', 'name="description"', '>'.$Blog->description.'</textarea>']);
    }

    public function testXItemWithObject(){
        $Blog = [
            'title'=>'Questions for hard work enthusiasts',
            'description'=>'Does everyone need a job, when machines can do it all?'
        ];
        $view = <<<VIEW
            <span>
                <section>
                    <div>
                        <x-item item="Blog">
                            <h1 prop="title"></h1>
                            <p prop="description"></p>
                        </x-item>
                    </div>
                </section>
            </span>
        VIEW;

        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;

        $args = ['Blog'=>$Blog];

        // echo $view;
        // exit;

        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        echo $viewEvaled;

        $this->str_not_contains($viewEvaled, ['<x-item>','</x-item>']);
        $this->str_contains($viewEvaled, '<h1>'.$Blog->title.'</h1>');
        $this->str_contains($viewEvaled, '<p>'.$Blog->description.'</p>');
        $this->str_contains($viewEvaled, ['<span>','<section>','<div>']);

    }

    /**
     * @test if we can display a node
     * @test if we can apply a filter
     */
    public function testNodeWithObjectAndPropToFilter(){
        $Blog = [
            'body'=>'# Is police funding a form of welfare?',
            'bodyTarget'=>'<h1>Is police funding a form of welfare?</h1>',
        ];
        $view = <<<VIEW
            <section item="Blog">
                <div prop="body" filter="commonmark:markdownToHtml"></div>
            </section>
        VIEW;
        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;
        // $lia = new \Liaison();
        // $lia->addApi('phad:filter.get', function(){return null;});
        $args = ['Blog'=>$Blog];

        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        echo $viewEvaled;

        $this->str_contains($viewEvaled, '<section>');
        $this->str_contains($viewEvaled, $Blog->bodyTarget);
        $this->str_not_contains($viewEvaled, ['item="Blog"', 'prop="body"', 'filter=']);

    }

    /**
     * @test that we can display a node after compiling & including phad
     */
    public function testNodeWithObject(){
        $Blog = [
            'title'=>'Are kids still in cages?',
            'description'=>'Feb 20, 2021... It seems so :('
        ];
        $view = $this->viewSrc('Other/NoAccess');
        $compiler = new \Phad\TemplateCompiler($view);
        $output = $compiler->compile($view, $this->template);
        $view = $output;

        $phad = new \Phad();
        $phad_block = \Phad\Blocks::VIEW;
        $args = ['Blog'=>$Blog];

        // echo $view;
        // exit;
        ob_start();
        eval('?>'.$view);
        $viewEvaled = ob_get_clean();

        echo $viewEvaled;

        $this->str_contains($viewEvaled, '<div>');
        $this->str_contains($viewEvaled, '</div>');
        $this->str_contains($viewEvaled, '<h1>'.$Blog->title.'</h1>');
        $this->str_contains($viewEvaled, '<p>'.$Blog->description.'</p>');
        $this->str_not_contains($viewEvaled, ['item=','prop=']);
    }




    protected function itemList(&$BlogList){
        $BlogList = [
            [
                'title'=>'Cats',
                'description'=>'Cats are furry and cute and maybe a little evil',
                'type'=>'pet',
            ],
            [
                'title'=>'Dogs',
                'description'=>'Dogs are furry and cute and maybe a little dumb',
                'type'=>'pet',
            ],
            [
                'title'=>'Bears',
                'description'=>'Bears are the best, but they aren\'t trained for cuddles :(',
                'type'=>'wild',
            ]
        ];
    }
}
