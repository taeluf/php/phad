<?php 

namespace Phad\Test\Integration;

/**
 * This class for testing views in general
 */
class Views extends \Phad\Tester {
    
    public function testDataHandler(){
        $this->phad = $this->phad();
        $this->phad->data_loaders['phad:goop'] = 
            function($node, $ItemInfo){
                return [
                    ['title'=>'One', 'summary'=>"first summary"],
                    ['title'=>'Two', 'summary'=>"second summary"],
                ];
            }
        ;

        $item = $this->item('Other/DataLoader');

        
        $this->str_contains($item,
            '<h1>One</h1>',
            '<p>first summary</p>',
            '<h1>Two</h1>',
            '<p>second summary</p>',
        );
    }

    public function phad(){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        return $phad;
    }
}
