<?php

namespace Phad\Test\Integration;

/**
 * Tests sitemap creation
 */
class Sitemap extends \Phad\Tester {

    /**
     * @test that attributes on the <sitemap> node make it into the sitemap entry. Attributes like 'priority', 'last_mod', and 'changefreq'
     */
    public function testSitemapAttributesFromDeclaration(){
        $h = $this->helper();
        $ldb = $h->ldb();
        $lia = $h->lia();
        $phad = $this->phad();
        $phad->pdo = $h->pdo();
        $blogs = $h->getBlogs(6, true, ['title','description','type', 'slug']);
        $h->insertBlogs($blogs);

        // $phad->sitemap_handlers['namespace:blogSitemapHandler'] =
        //     function($entry, $row, $sitemapData) {
        //         $entry['loc'] = $entry['loc'].'/yep/';
        //         return $entry;
        //     };

        $view = $phad->item('Sitemap/QueryWithDeclaredSitemapAttributes');
        $sitemapData = $view->sitemap_data();
        $phad->sitemap->make_sitemap($sitemapData);


        $this->test('Sitemap Build With all data');
            $this->compare(
                $ldb->query("SELECT 1354 as lastmod, ('/blog/' || slug ) AS loc, 0.8 AS priority, 'daily' as changefreq FROM blog"),
                $phad->sitemap->get_stored_entries('sitemap') 
            );
    }

    /**
     * @test sitemap handler actually modifies the sitemap entry
     */
    public function testSitemapHandlerActive(){
        echo "\n\nThis test disabled because sitemap handlers have been removed from the refactored sitemap class (jan 3, 2022)\n\n";
        $this->disable();
        return;
        $h = $this->helper();
        $ldb = $h->ldb();
        $phad = $this->phad();
        $phad->pdo = $h->pdo();
        $blogs = $h->getBlogs(6, true, ['title','description','type', 'slug', 'last_mod', 'search_priority']);
        $h->insertBlogs($blogs);

        $phad->sitemap_handlers['namespace:blogSitemapHandler'] = 
            function($entry, $row, $sitemapData) {
                $entry['loc'] = $entry['loc'].'/yep/';
                return $entry;
            };

        $view = $phad->item('Sitemap/QueryWithHandlerAndAttribute');
        $sitemapData = $view->sitemap_data();
        $builder = $phad->sitemap->make_sitemap($sitemapData);


        $this->test('Sitemap Build With all data');
            $this->compare(
                $ldb->query("SELECT last_mod as lastmod, ('/blog/' || slug || '/yep/') AS loc, 0.85 AS priority FROM blog"),
                $builder->get_stored_entries('sitemap') 
            );
    }

    /**
     * @test exception thrown when a sitemap handler is attempted, but not set
     */ 
    public function testSitemapHandlerDeclaredButNotSet(){

        echo "\n\nThis test disabled because sitemap handlers have been removed from the refactored sitemap class (jan 3, 2022)\n\n";
        $this->disable();
        return;
        $h = $this->helper();
        $phad = $this->phad();
        $phad->pdo = $h->pdo();
        $ldb = $h->ldb();
        $blogs = $h->getBlogs(6, true, ['title','description','type', 'slug', 'last_mod', 'search_priority']);

        $h->insertBlogs($blogs);

        $view = $phad->item('Sitemap/QueryWithHandlerAndAttribute');
        $sitemapData = $view->sitemap_data();


        $this->catch('Exception')->containing('There was no handler found for \'namespace:blogSitemapHandler\'');

        try {
            $builder = $phad->sitemap->make_sitemap($sitemapData);
        } catch (\Exception $e){
            $this->throw($e);
        }
    }

    /**
     * @test that sitemap data is correctly built from the sitemap info + database item entries
     */
    public function testBuildSitemap(){
        $this->empty_dir($this->file('test/input/sitemap'));
        $h = $this->helper();
        $phad = $this->phad();
        $phad->pdo = $h->pdo();
        // $phad->sitemap_handlers['namespace:blogSitemapHandler'] = function($v){return $v;};
        // $ldb = $h->ldb();
        $ldb = new \Tlf\LilDb($phad->pdo);
        $blogs = $h->getBlogs(6, true, ['title','description','type', 'slug', 'last_mod', 'search_priority']);
        $h->insertBlogs($blogs);

        $view = $phad->item('Sitemap/QueryWithHandlerAndAttribute');
        $sitemapData = $view->sitemap_data();

        $builder = $phad->sitemap->make_sitemap($sitemapData, 'build-sitemap');
        $this->test('Sitemap Build With all data');
            $this->compare_arrays(
                $ldb->query("SELECT last_mod as lastmod, ('/blog/' || slug) AS loc, 0.85 AS priority FROM blog"),
                $phad->sitemap->get_stored_entries('build-sitemap') 
            );

        // echo "\n\n\n\n\n--------\n\n";
        // var_dump(
        //         $ldb->query("SELECT '' AS changefreq, last_mod, ('/blog/' || slug) AS loc, 0.85 AS priority FROM blog"),
        // );
        //
        // echo "\n--------\n\n";
        // var_dump(
        //     $phad->sitemap->get_stored_entries('build-sitemap')
        // );
    }


    /**
     * @test that genericized sitemap data is correctly extracted from a <sitemap> node
     */
    public function testSitemapNodeData(){
        $phad = $this->phad();

        $view = $phad->item('Sitemap/QueryWithHandlerAndAttribute');
        $sitemapData = $view->sitemap_data();
        $this->test('Presence of Sitemap Data');
            $this->compare(
                ['/blog/{slug}'=> [
                    'sql'=>'SELECT slug, last_mod, search_priority/100.00 AS priority FROM blog',
                    'handler'=>'namespace:blogSitemapHandler',
                    'priority'=>'0.8',
                    'pattern'=>'/blog/{slug}',
                    ],
                ],
                $sitemapData
            );
    }

    public function phad(){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        $phad->sitemap = new \Phad\SitemapBuilder($this->file('test/input/sitemap/'));
        $phad->router = new \Lia\Addon\Router();
        $phad->sitemap->router = &$phad->router;
        $phad->sitemap->pdo = &$phad->pdo;
        $phad->sitemap->throw_on_query_failure = true;
        return $phad;
    }


    // public function pdo(){
        // return new \PDO("sqlite::memory:");
    // }
}
