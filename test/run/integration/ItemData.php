<?php

namespace Phad\Test\Integration;

/**
 * Tests for getting data from an item's view as an array
 */
class ItemData extends \Phad\Tester {

    public function testNestedObjects(){
        echo "\n\n\n-----------\n\n";
        echo "This test disabled because I've opted for a simpler data approach that just returns the rows of the first item ... rather than building a stack from all the items.";
        echo "\n\nI intend to change this in the future.";
        $this->disable();
        return;
        // 1. stack head is $stack->data
        // 2. start blog item. head is $stack->data['BlogList']
        // 3. start blog row. head is $stack->data['BlogList'][0]
            // 4. start song item. head is $stack->data['BlogList'][0]['SongList']
            // 5. start song row. head is $stack->data['BlogList'][0]['SongList'][0]
            // 6. end song row. head is $stack->data['BlogList'][0]['SongList']
            // 7. end song item. head is $stack->data['BlogList'][0]
        // 4. end blog row. head is $stack->data['BlogList']
        // 5. end blog item. head is $stack->data
        $args = $this->items($Blog,$Song);
        $view = $this->item('Other/OneNestedItem', $args);

        $BlogData = (array)$Blog;
        $BlogData['SongList'] = [(array)$Song];
        $this->compare(
            ['BlogList'=>[$BlogData]],
            $view->rows()
        );
    }

    /**
     * @todo re-implement the stack to get data from multiple objects at once
     */
    public function testAdjacentObjects(){
        echo "\n\n\n-----------\n\n";
        echo "This test disabled because I've opted for a simpler data approach that just returns the rows of the first item ... rather than building a stack from all the items.";
        echo "\n\nI intend to change this in the future.";
        $this->disable();
        return;
        // 1. stack head is $stack->data
        // 2. start blog item. head is $stack->data['BlogList']
        // 3. start blog row. head is $stack->data['BlogList'][0]
        // 4. end blog row. head is $stack->data['BlogList']
        // 5. end blog item. head is $stack->data
        // 6. start song item. head is $stack->data['SongList']
        // 7. ... repeat 3-5, but with SongList.
        $args = $this->items($Blog, $Song);
        $view = $this->item('Other/AdjacentItems', $args);
        $data = $view->rows();

        $this->compare(['BlogList'=>[(array)$Blog], 'SongList'=>[(array)$Song]], $data);
    }

    public function testOneObject(){
        // 1. stack head is $stack->data
        // 2. start blog item. head is $stack->data['BlogList']
        // 3. start blog row. head is $stack->data['BlogList'][0]
        // 4. end blog row. head is $stack->data['BlogList']
        // 5. end blog item. head is $stack->data

        // return;
        $phad = $this->phad();
        $args = $this->rows($Blog);
        // return
        // print_r($args);
        // exit;
        // $view = $phad->item('Other/NoAccess', $args);
        // $data = $view->rows();
        $view = $phad->item('Other/NoAccess', $args);
        $output = $view->html();
        echo($output);
        $data = $view->rows();
        // print_r($phad->stack->data);

        print_r($data);

        $this->compare([$Blog], $data);
    }

    public function phad(){
        $phad = new \Phad();
        $phad->exit_on_redirect = false;
        $phad->force_compile = true;
        $phad->item_dir = $this->file('test/input/views/');
        $phad->sitemap = new \Phad\SitemapBuilder($this->file('test/input/sitemap/'));
        $phad->router = new \Lia\Addon\Router();
        $phad->sitemap->router = &$phad->router;
        $phad->sitemap->pdo = &$phad->pdo;
        $phad->sitemap->throw_on_query_failure = true;
        return $phad;
    }
}
