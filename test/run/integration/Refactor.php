<?php

namespace Phad\Test\Integration;

/**
 * For tests specifically useful in a refactor process
 */
class Refactor extends \Tlf\Tester {

    /**
     * @test that basic liaison integration works
     */
    public function testNewStructure(){
        echo "I don't think this test is useful any more ... but i'm leaving it for now, bc it might help me with setting up the server again.";
        $this->disable();
        return;
        $lia = new \Lia();
        $server = \Lia\Package\Server::main($lia);
        $router = $lia->addon('lia:server.router');

        //setup phad
        $options = [
            'item_dir'=>$this->file('test/input/views/'),
            'cache_dir'=>$this->file('test/input/cache/'),
            'sitemap_dir'=>$this->file('test/input/sitemap/'),
            'pdo' => new \PDO('sqlite::memory:'),
            'user' => new class(){},
            'router' => $router,

            'force_compile'=>true,
        ];
        $phad = \Phad::main($options);
        // $phad->force_compile = true;
        $item = $phad->item('Other/NoAccess',
            ['Blog'=> ['title'=>'cat','description'=>'fur babe'] ]
        );

        $this->str_contains($item->html(), ['<h1>cat</h1>', '<p>fur babe</p>']);
    }

}
