<?php

namespace Phad;

class TestHelper {

    protected $lia;
    protected $package;
    protected $compo;
    protected $pdo;
    protected $didCreateBlog = false;
    public $lilDb;



    public function lia(){
        if ($this->lia==null){
            $this->lia = $this->lia ?? new \Lia();
            $server = new \Lia\Package\Server($this->lia);
        }
        return $this->lia;
    }

    public function ldb(){
        $this->lilDb = $this->lilDb ?? new \Tlf\LilDb($this->pdo());
        return $this->lilDb;
    }

    public function pdo($throwHappy=false){
        $this->pdo = $this->pdo ?? new \PDO('sqlite::memory:');
        if ($throwHappy)$this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $this->pdo;
    }

    public function insertBlogs($countOrBlogsList){
        $blogs = is_array($countOrBlogsList) ? $countOrBlogsList : $this->getBlogs($countOrBlogsList);
        $ldb = new \Tlf\LilDb($this->pdo());

        if (!$this->didCreateBlog){
            $cols = [
                'title'=>'VARCHAR(254)', 
                'description'=>'VARCHAR(1000)',
                'type'=> 'VARCHAR(254)',
                'slug'=> 'VARCHAR(254)',
                'last_mod'=> 'int',
                'search_priority'=>'int',
            ];
            $createCols = [];
            foreach (array_keys((array)$blogs[0]) as $col){
                $createCols[$col] = $cols[$col];
            }
            $ldb->create('blog', $createCols);
            $this->didCreateBlog = true;
        }
        $ldb->insertAll('blog', $blogs);
    }

    public function getBlogs($count, $asObj=true, $cols=['title', 'description', 'type']){
        $i = 0;
        
        $BlogList = [
            [
                'title'=>'Cats',
                'description'=>'Cats are furry and cute and maybe a little evil',
                'type'=>'pet',
            ],
            [
                'title'=>'Dogs',
                'description'=>'Dogs are furry and cute and maybe a little dumb',
                'type'=>'pet',
            ],
            [
                'title'=>'Bears',
                'description'=>'Bears are the best, but they aren\'t trained for cuddles :(',
                'type'=>'wild',
            ],
            [
                'title'=>'Lions',
                'description'=>'Lions are furry and cute scary',
                'type'=>'wild',
            ],
            [
                'title'=>'Tigers',
                'description'=>'Tigers are big cats',
                'type'=>'wild',
            ],
            [
                'title'=>'Human',
                'description'=>'Humans are big smart dumb small animals',
                'type'=>'human',
            ]
        ];

        foreach ($BlogList as $bIndex=>$b){
            $BlogList[$bIndex]['last_mod'] = time();
            $BlogList[$bIndex]['slug'] = str_replace(' ', '-', strtolower($b['title']));
            $BlogList[$bIndex]['search_priority'] = 85;
        }
        
        shuffle($BlogList);
        $out = [];
        foreach ($BlogList as $b){
            if ($i++>=$count)break;
            $obj = [];
            foreach ($cols as $c){
                $obj[$c] = $b[$c];
            }
            $out[] = $asObj ? (object)$obj : $obj;
        }
        return $out;
    }

}
