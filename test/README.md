# Tests README

## How to set up new phad tests:
1. create a class that extends `\Phad\Tester`
2. create a method `public function phad()` that returns a valid phad instance
3. call `$this->item('item/name', $args)` to load an item
4. do comparisons and stuff
