<?php
// return;
/**
 * Sets up the server before running tests
 * this is NOT part of RUNNING the server
 * it just resets the databases & stuff
 */

$_SERVER['REQUEST_URI'] = '/';

$db_file = __DIR__.'/Server/db.sqlite';
unlink($db_file);
$lildb = \Tlf\LilDb::sqlite($db_file);

require(__DIR__.'/Server/db-blogs.php');
require(__DIR__.'/Server/db-characters.php');
require(__DIR__.'/Server/db-documents.php');
require(__DIR__.'/Server/db-candelete.php');

$tester = new \Tlf\Tester();
$tester->empty_dir(__DIR__.'/Server/files-uploaded/');

$tester->empty_dir(__DIR__.'/Server/cache/');


$phad = new \Phad();
$phad->item_dir = __DIR__.'/Server/phad/';
$phad->cache_dir = __DIR__.'/Server/cache/';

$phad->compile_all_items();
$target = $phad->routes_from_cache();

$lia = new \Lia();
$server = \Lia\Package\Server::main($lia);
$router = $lia->addon('lia:server.router');

$dir = __DIR__.'/Server/';
require(__DIR__.'/Server/phad-setup.php');

// $phad->force_compile = true;
$phad->create_sitemap_file();

