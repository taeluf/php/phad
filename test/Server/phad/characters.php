<route pattern="/characters/"></route>

<div item="Character" >
    <p-data sql="SELECT character.*, species.name AS species FROM character AS Character JOIN species ON character.species = species.id"></p-data>
    <h2 prop="name"></h2>
    <p>Level: <?=$Character->level?></p>
    <p>Species: <?=$Character->species?></p>
</div>
