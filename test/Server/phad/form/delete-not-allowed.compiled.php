<?php 
if (($phad_block??null)===\Phad\Blocks::SITEMAP_META){
    return array (
);
}
?><?php 
if ($phad_block===\Phad\Blocks::ROUTE_META){
    return array (
  0 => 
  array (
    'pattern' => '/delete-not-allowed/',
  ),
);
}
?>

<?php
/** template originally developed on jan 20, 2022
 * @param \Phad $phad an object that handles the callback methods in the template. Can be any type, if you wish to change it from `\Phad`
 */






## becomes $BlogInfo
$CandeleteInfo = (object)[
    'name'=> 'Candelete', //becomes Blog
    'mode'=>$phad_block,
    'apis'=> array (
  0 => 'form',
  1 => 'create',
  2 => 'update',
), //rawdata generally json. could be sql or csv though
    'type'=>'form', //just view or form at this point
    'args'=>$args, 
    'data_index'=>false,
    'data_nodes' => array (
  0 => 
  array (
    'type' => 'default',
  ),
), // each contains access="role:admin" & sql="Select * ...", etc
    'response_code' => false, // could start as 500?
    'rows'=>[], // could start null/false?
    'errors'=>[], // just gets appended to by whatever when there's a problem
    'submit_errors'=>[],
    'candelete' => 'call:never_allow',
    'diddelete' => 'print:it did delete...',
    'cansubmit' => false,
    
];


$CandeleteInfo->properties = 
    array (
  'title' => 
  array (
    'type' => 'text',
    'maxlength' => '75',
    'tagName' => 'input',
  ),
  'id' => 
  array (
    'tagName' => 'input',
    'type' => 'hidden',
  ),
);



$phad->item_initialized($CandeleteInfo);



if ($phad_block == \Phad\Blocks::FORM_DELETE){
    if ($phad->can_delete($CandeleteInfo)){
        
        $did_delete = $phad->delete($CandeleteInfo);
        if ($did_delete){
            
            $phad->did_delete($CandeleteInfo);
            return;
        }
    }
}



if ($phad_block==\Phad\Blocks::ITEM_META){
    return $CandeleteInfo;
}

foreach ($CandeleteInfo->data_nodes as $_index=>&$_node){
    $_node['index'] = $_index;
    if (isset($args[':data'])){
        if (!isset($_node['name'])||$_node['name']!=$args[':data']){
            continue;
        }
    }
    if (isset($_node['if'])){
        //@todo templatize p-data 'if' attribute instead of using `eval()`
        $eval_code = 'return ('.$_node['if'].');';
        $result = eval($eval_code);
        if (!$result){
            $_node['response_code'] = 403;
            $CandeleteInfo->response_code = 403;
            continue;
        }
    }
    if (!$phad->can_read_data($_node, $CandeleteInfo)){
        $CandeleteInfo->response_code = 403;
        $_node['response_code'] = 403;
        continue;
    }
    // may include getting $args['Blog'] or $args['BlogList'] instead of using a query
    $CandeleteInfo->rows = $phad->read_data($_node, $CandeleteInfo);
    if (count($CandeleteInfo->rows)==0){
        $CandeleteInfo->response_code = 404;
        $_node['response_code'] = 404;
        continue;
    }
    $CandeleteInfo->response_code = 200;
    $_node['response_code'] = 200;
    $CandeleteInfo->data_index = $_index;
    break;
}

if ($phad_block == \Phad\Blocks::ITEM_DATA){
    // does not handle nested items. I'm okay with that, for now ... i can always make separate item views
    return $phad->get_rows($CandeleteInfo);
}




if (count($CandeleteInfo->rows)===0){
    $phad->no_rows_loaded($CandeleteInfo);
}

foreach ($CandeleteInfo->rows as $RowIndex_Candelete => $CandeleteRow ): 
    $Candelete = $phad->object_from_row($CandeleteRow, $CandeleteInfo);
    
        if ($CandeleteInfo->mode == \Phad\Blocks::FORM_SUBMIT){
            // <onsubmit> code goes here. Set `$CandeleteInfo->mode = null` stop submission & display the form.
            
            if ($CandeleteInfo->mode==\Phad\Blocks::FORM_SUBMIT){
                if ($phad->can_submit($CandeleteInfo, $CandeleteRow)
                    &&$phad->submit($CandeleteInfo, $CandeleteRow)){
                    
                    $phad->redirect($CandeleteInfo,$CandeleteRow);
                    return;
                } else {
                    
                }
            }
            $args['CandeleteSubmitErrorsList'] = $CandeleteInfo->submit_errors;

            //  = $CandeleteInfo->
            // print_r($args);
        }
    

    
    ?><form action="" method="POST">
    <div class="errors">
<?php foreach ($CandeleteInfo->submit_errors as $e){
    echo "\n        <p>".$e['msg']."</p>";
}?>  
</div>

    <input type="text" name="title" maxlength="75" value="<?=$Candelete->title?>">
</form><?php
endforeach;

?>

