<route pattern="/nested/"></route>

<div item="Species" >
<h2 prop="name"></h2>
    <?php $args['species'] = $Species->id; ?>
    <x-item item="Character">
        <p-data where="Character.species = :species"></p-data>
        <b prop="name"></b>
    </x-item>
</div>
