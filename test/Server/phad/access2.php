<route pattern="/access2/"></route>

<h1>Before Item Node</h1>
<div item="Blog" >
    <p-data access="role:admin;call:permit_me" limit="1">
        <on s=404>Blogs Not Found</on>
        <on s=200>Access Granted</on>
        <on s=403>User does not have admin role. User has role <?=$_GET['user']?></on>
    </p-data>
    <!-- access-granted -->
    <h1 prop="title"></h1>
    <div prop="body"></div>
</div>
<h2>After Item Node</h2>
