<?php

$lildb->create('character',
    [
        'id' => 'integer PRIMARY KEY AUTOINCREMENT',
        'name' => 'varchar(100)',
        'level' => 'integer',
        'species' => 'integer',
    ]
);

$lildb->create('species',
    [
        'id'=>'integer UNIQUE',
        'name'=>'varchar(100)',
    ]
);

$lildb->insert('species',
    ['id'=>1,
    'name'=>'Bear',
    ]
);

$lildb->insert('species',
    ['id'=>2,
    'name'=>'Goose',
    ]
);



$lildb->insert('character',
    ['name'=>'Reed',
    'species'=>1,
    'level'=>10,
    ]
);

$lildb->insert('character',
    ['name'=>'Jalin',
    'species'=>1,
    'level'=>10,
    ]
);

$lildb->insert('character',
    ['name'=>'PooPooMaShu',
    'species'=>2,
    'level'=>10,
    ]
);

$lildb->insert('character',
    ['name'=>'Baroney',
    'species'=>2,
    'level'=>10,
    ]
);


