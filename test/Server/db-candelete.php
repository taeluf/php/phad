<?php

$lildb->create('candelete',
    [
        'id' => 'integer PRIMARY KEY AUTOINCREMENT ',
        'title' => 'varchar(100)',
    ]
);

$lildb->insert('candelete',
    [
        'id'=>1,
        'title'=>'I like bears a lot',
    ]
);


$lildb->insert('candelete',
    [
        'id'=>2,
        'title'=>'Not Lorem Ipsum',
    ]
);

$lildb->insert('candelete',
    [ 'id'=>3,
        'title'=>'Baby Fires',
    ]
);
