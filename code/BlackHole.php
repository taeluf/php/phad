<?php

namespace Phad;

#[\AllowDynamicProperties]
class BlackHole {
    public function __call($method,$args){
        return $this;
    }
    public function __get($param){
        return null;
    }
    public function __set($param, $value){
        $this->$param = $value;
    }
    public function __toString(){
        return '';
    }

    public function offsetGet($key){
        return null;
    }
    public function offsetUnset($key){
    }
}

