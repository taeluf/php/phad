<?php

namespace Phad;

class Blocks {

    const VIEW = 0;
    const ROUTE_META = 1;
    const SITEMAP_META = 2;
    const ITEM_META = 3;
    const ITEM_DATA = 4;
    const FORM_DELETE = 5;
    const FORM_SUBMIT = 6;
    const FORM_ERRORS = 7;

}
