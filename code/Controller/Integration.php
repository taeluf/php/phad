<?php

namespace Phad;

/**
 * convenience class to help setup phad with liaison
 */
class Integration {

    public $phad;

    public bool $force_compile;
    
    public ?\Lia $lia = null;

    public function setup_liaison_routes($lia){
        $phad = $this->phad;
        $this->lia = $lia;

        // $items = $phad->get_all_items();
        // $routes = $phad->routes_from_items($items, __DIR__.'/phad/');
        $routes = $phad->routes_from_cache($this->force_compile);
        // $routes = $phad->routes_from_cache(true);

        foreach ($routes as $pattern=>$item_name){

            ///////
            // Maybe I should check view information here to determine whether i give a form route or a view route
            // I'd also really like to just route to a file (bc then I can cache the route, use less cpu & memory), but that requires compilation & caching ... so let's not rush it.
            ///////
            
            // echo "\nPattern:".$pattern;


            //@POST is necessary to submit forms, unless I do this smarter style
            // $lia->addRoute("@GET.@POST.".$pattern,
            //     function(\Lia\Obj\Route $route, $response) use ($item_name, $phad){
            //
            //         $phad_response = $phad->get_response($route->paramaters(), $item_name);
            //
            //         $response->content = $phad_response['content'];
            //         if (!empty($phad_response['headers'])){
            //             $response->headers = $phad_response['headers'];
            //         }
            //     }
            // );
            $full_pattern = "@GET.@POST.".$pattern;
            // var_dump($full_pattern);
            // exit;
            $lia->addRoute($full_pattern, [$this,'handle_liaison_route']);
        }
    }

    public function setup_liaison_route($lia, $url, $file){
        $lia->addRoute($url,$file);
    }

    public function handle_liaison_route(\Lia\Obj\Route $route, $response){
        $phad = $this->phad;
        $routes = $phad->routes_from_cache($this->force_compile);

        // remove @GET.@POST. from the beginning
        $pattern = substr($route->paramaterizedPattern(), 11);

        $item_name = $routes[$pattern];
        $phad_response = $phad->get_response($route->paramaters(), $item_name);

        $item = $phad_response['view'];

        // print_r($item->resource_files());
        // exit;
        foreach ($item->resource_files() as $type=>$list){

            foreach ($list as $path){
                $this->lia->addResourceFile($path);
            }
        }


        $response->content = $phad_response['content'];
        if (!empty($phad_response['headers'])){
            $response->headers = $phad_response['headers'];
        }

    }

}
