<?php

namespace Phad;

/**
 * See the stack unit tests to understand how this works
 *
 * I'm keeping this class around because I intend to re-implement in the future & it was such a pain to make the first time round
 *
 * @todo i should rename the functions to be a little more generic, maybe?
 */
class Stack {

    /**
     * Always points to the top of $stack
     */
    public $head;

    public $data = [];

    public $stack = [];

    public function __construct(){
        $this->head = &$this->data;
        $this->stack[] = &$this->data;
    }

    public function itemListStarted($ItemInfo){
        $key = $ItemInfo->name.'List';
        $this->head[$key] = [];
        $this->head = &$this->head[$key];
        $this->stack[] = &$this->head;
    }
    public function rowStarted($ItemInfo, $Item){
        $item_row = (array)$Item;
        $this->head[] = &$item_row;
        $this->head = &$item_row;
        $this->stack[] = &$this->head;

    }

    public function rowFinished($ItemInfo, $Item){
        array_pop($this->stack);
        $this->head = &$this->stack[count($this->stack)-1];
    }
    public function itemListFinished($item){
        $prev = array_pop($this->stack);
        $this->head = &$this->stack[count($this->stack)-1];
    }

}
