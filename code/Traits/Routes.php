<?php

namespace Phad;

trait Routes {

    protected ?array $routes = null; 

    /**
     * get a list of all routes from the cache. if $force_compile is true, then compile new routes
     * @param $force_compile true to always compile routes. false to load from cache (if exists)
     */
    public function routes_from_cache(bool $force_compile = false){
        $cache = $this->cache_dir.'/phad-routes.php';

        if ($force_compile===false&&$this->routes!=null){
            return $this->routes;
        }

        if ($force_compile || !file_exists($cache)){
            if (!is_dir($this->cache_dir))mkdir($this->cache_dir);
            $routes = $this->routes_from_items($this->get_all_items());
            file_put_contents($cache, 
                '<?php return ' 
                    .var_export($routes, true)
                ."\n;"
            );
            $this->routes = $routes;
            return $routes;
        }

        $this->routes = require($cache);
        return $this->routes;
    }

    public function get_all_items(){
        $dir = $this->item_dir;
        $files = \Lia\Utility\Files::all($dir, $dir, '.php');

        $filtered = [];
        foreach ($files as $f){
            $str = '.compiled.php';
            if (substr($f, -strlen($str))==$str)continue;
            //remove leading slash & trailing .php
            $filtered[] = substr($f,1,-4);
        }

        return $filtered;
    }

    public function routes_from_items(array $item_names) {
        $item_dir = $this->item_dir;
        $route_list = [];
        foreach ($item_names as $vn){
            $item = $this->item($vn);

            $item = new \Phad\Item($vn, $item_dir, []);
            $item->force_compile = $this->force_compile;
            $routes = $item->routes();
            if (count($routes)==0)continue;

            $item_routes = [];
            foreach ($routes as $r){

                $pattern = $this->route_prefix . $r['pattern'];
                $pattern = str_replace('//','/',$pattern);
                $item_routes[$pattern] = $vn;
            }

            $route_list = array_merge($route_list, $item_routes);
        }


        return $route_list;
    }

    public function get_response($item_args, $item_name){
            $response = []; 

            $item_args['is_route'] = true;
            $item = $this->item($item_name,$item_args);
            $response['view'] = $item;
            //check if it's a form
            $data = $item->info();

            if ($_SERVER['REQUEST_METHOD']=='POST'&&
                    ($data->type!='form' && strpos($item->routes()[0]['pattern'], '@POST.')===false)
            ){
                $response['content'] = "This page is not available via POST.";
                return $response;
            } else if ($_SERVER['REQUEST_METHOD']=='GET'
                &&($_GET['phad_action']??null)=='delete'
                &&$data->type=='form'){
                $item->args['id'] = $item->args['id'] ?? $_GET['id'] ?? null;
                $response['content'] = $item->delete();
                // $response['headers'] = $this->headers;
                return $response;
            } else if ($_SERVER['REQUEST_METHOD']=='POST'&&$data->type=='form'){
                // $item->args = array_merge($item->args, $_POST);
                $response['content'] = $item->submit();
                // $response['headers'] = $this->headers;
                return $response;
            }
            // else if ($_SERVER['REQUEST_METHOD']=='GET'&&$data->type=='form'){
            //     $response->content = $item->delete($_GET);
            // }

            //display json
            if (($_GET['get']??null)=='json'){
                $data = $item->rows();
                if (($_GET['prettyprint']??false)=='true'){
                    $response['content'] = json_encode($data, JSON_PRETTY_PRINT);
                } else {
                    $response['content'] = json_encode($data);
                }
                // $response->content = print_r($data,true);
                $response['useTheme'] = false;
                return $response;
            }

            if (is_object($data)
                &&$data->type == 'form'
            ){
                // $item->args = array_merge($_GET, $item->args);
                $response['content'] = $item->html();
                // $response['content'] = $item->html_with_no_data();
                return $response;
            }

            $response['content'] = ''.$item;

            return $response;
    }
}
