<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/old/Stack.php  
  
# class Phad\Stack  
See the stack unit tests to understand how this works  
  
I'm keeping this class around because I intend to re-implement in the future & it was such a pain to make the first time round  
  
See source code at [/code/old/Stack.php](/code/old/Stack.php)  
  
## Constants  
  
## Properties  
- `public $head;` Always points to the top of $stack  
- `public $data = [];`   
- `public $stack = [];`   
  
## Methods   
- `public function __construct()`   
- `public function itemListStarted($ItemInfo)`   
- `public function rowStarted($ItemInfo, $Item)`   
- `public function rowFinished($ItemInfo, $Item)`   
- `public function itemListFinished($item)`   
  
