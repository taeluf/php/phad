<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/BlackHole.php  
  
# class Phad\BlackHole  
  
See source code at [/code/BlackHole.php](/code/BlackHole.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __call($method,$args)`   
- `public function __get($param)`   
- `public function __set($param, $value)`   
- `public function __toString()`   
- `public function offsetGet($key)`   
- `public function offsetUnset($key)`   
  
