<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Phad.php  
  
# class Phad  
  
See source code at [/code/Phad.php](/code/Phad.php)  
  
## Constants  
  
## Properties  
- `public \PDO $pdo = null;`   
- `public $route_prefix = '';`   
- `public array $configs = [];` array of configs, typically from an on-disk json file. These are not directly used by Phad, but may be useful to subclasses or integrations.  
- `public \Phad\SitemapBuilder $sitemap;` sitemap builder instance  
- `public \Lia\Addon\Router $router;` a router instance  
- `public $handlers = [  
          
        'user_has_role'=>null,  
          
        'item_initialized'=>null,  
    ];` Callback handlers. Key should be the handler name & value should be a callable  
- `public $data_loaders = [];` array of callables that return an array of rows.  
Each callable should accept args `(DomNode, ItemInfo)` & return an array of rows.  
- `public bool $exit_on_redirect = true;` set false to stop phad from `exit`ing when calling `->redirect()`   
You can custom handle the `header()` call by creating a `header()` function in the `Phad` namespace.   
  
See https://akrabat.com/replacing-a-built-in-php-function-when-testing-a-component/ to understand the `Phad\header()` thing   
- `public bool $force_compile = false;` true to always re-compile views  
- `public $global_phad_args = [];` args to pass to every phad view  
- `public $filters = [  
            ];` `key=>value` array of filters where `key` is the filter you write in the html & `value` is a callable  
- `public $item_dir;` Absolute path to a directory that contains phad items  
- `public $cache_dir;` Absolute path to a directory to store cached files  
- `public $sitemap_dir;` Dir to write sitemap.xml file to  
- `public bool $throw_on_query_failure = false;` true to throw exception when query failes. false to silently fail & return false  
- `public $integration;` an object that is used to make phad work with liaison  
- `public $access_handlers = [];` array of functions that handle access operations  
- `public $sitemap_handlers = [];` array of handlers for sitemap building  
- `public $validators = [];` array of validation functions  
  
## Methods   
- `public function __construct()`   
- `public function __call($method,$args)`   
- `public function set_handler(\Phad\Handler $handler)` Use the given handler for hooks phad requires you to handle  
- `public function read_data($node, $ItemInfo)`   
- `public function modify_query_info(array $query_info)` Modify a query before getting rows  
  
- `public function object_from_row(array $row, $ItemInfo)` @override  
- `public function has_item($name)`   
- `public function item($name, $args=[])`   
- `public function item_from_file(string $file_path, array $args=[])` get an item instance from a file  
  
This does not set up any routing  
- `public function filter(string $filterName, $value)`   
- `public function filter_markdown($markdown)` Apply commonmark conversion to the value, turning markdown into html  
- `public function get_rows($ItemInfo)`   
- `static public function main($options = [], $custom_phadnull)` Just boilerplate to make phad easier to initialize  
- `public function create_sitemap_file(): string` Make a sitemap file from all views  
- `public function compile_all_items()`   
- `public function call($handler, ...$args)` call a method that's defined in strings like `call:check_is_2022` (would call `$this->access_handler['check_is_2022']` if it is set   
  
- `public function parse_functions(string $call_string)` parse a string like `print:woohoo;call:somethin;role:admin;`  
- `public function no_rows_loaded(stdClass $ItemInfo)` Handle when no rows were loaded. This method is for overriding and does literally nothing otherwise.  
  
- `public function read_node_failed(array $node)` Handle when a node cannot be read  
  
- `public function sanitize_user_row(array $row)` replace each `$value` with `htmlspecialchars($value)`  
  
