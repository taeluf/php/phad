<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Controller/Query.php  
  
# class Phad\Query  
Grants access to an item's node & loads item list  
See source code at [/code/Controller/Query.php](/code/Controller/Query.php)  
  
## Constants  
  
## Properties  
- `public \PDO $pdo;`   
- `public $throw_on_query_failure = false;` true to throw exception when query failes. false to silently fail & return false  
  
## Methods   
- `public function __construct()`   
- `public function get(string $item_name, $query_info, $args=[], $item_type'view', $data_nodenull)`   
- `public function buildSql($item_name, $query_info, $args, $binds)`   
  
