<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Controller/SitemapBuilder.php  
  
# class Phad\SitemapBuilder  
A simple Sitemap builder that writes to disk immediately so memory use stays low.  
See source code at [/code/Controller/SitemapBuilder.php](/code/Controller/SitemapBuilder.php)  
  
## Constants  
  
## Properties  
- `public $cache_dir;`   
- `public $dir;`   
- `public $handles = [];` file handles  
- `public $handlers = [];` sitemap handlers that modify sitemap data  
- `public $router;` a router object that is used to parse the url patterns  
- `public \PDO $pdo;` A pdo instance for performing queries  
- `public bool $throw_on_query_failure = false;`   
- `public string $host = '';` the website to prefix all <loc> paths with, such as https://example.com  
  
## Methods   
- `public function __construct($storageDir)`   
- `protected function handle($name)` get an existing file handle or open a new one  
- `public function close($name)` Close any open file handles  
- `public function addEntry($sitemapName, $entry)` Writes a new entry to the target xml file  
- `public function get_stored_entries($sitemapName)` Parse the stored xml file for each url entry  
- `public function __destruct()`   
- `public function build_entries(array $sitemap_data)`   
- `public function get_results($sql)`   
- `public function parse_pattern($pattern)`   
- `public function fill_pattern($parsed, array $values)`   
- `public function make_sitemap(array $sitemap_data_list, $sitemap_name='sitemap')`   
- `public function get_sitemap_as_array(array $sitemap_data_list)`   
- `public function get_sitemap_list($items, $phad)`   
  
