<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Controller/Integration.php  
  
# class Phad\Integration  
convenience class to help setup phad with liaison  
See source code at [/code/Controller/Integration.php](/code/Controller/Integration.php)  
  
## Constants  
  
## Properties  
- `public $phad;`   
- `public bool $force_compile;`   
- `public \Lia $lia = null;`   
  
## Methods   
- `public function setup_liaison_routes($lia)`   
- `public function setup_liaison_route($lia, $url, $file)`   
- `public function handle_liaison_route(\Lia\Obj\Route $route, $response)`   
  
