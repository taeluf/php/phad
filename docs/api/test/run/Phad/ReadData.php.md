<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Phad/ReadData.php  
  
# class Phad\Test\Phad\ReadData  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function read_data(string $msg, array $expect, array $node, array $args=[], \PDO $pdonull)` Execute `$phad->read_data()` and test the results  
  
- `public function insert(array $rows)` create `blog` table and insert rows  
- `public function testReadDataQuery()`   
- `public function testReadDataNoQuery()`   
  
