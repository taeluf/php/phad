<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Phad/CanReadData.php  
  
# class Phad\Test\Phad\CanReadData  
  
  
## Constants  
  
## Properties  
- `public $user = null;` the active user role. Used in @see(can_read())  
  
## Methods   
- `public function user_has_role($role)` The `user_has_role` hanler that `Phad` calls during `can_read_data()`  
- `public function can_read(bool $expect, array $node, $msg, array $ItemInfo=[])` Set `$this->user = 'guest'` or `$this->user = 'admin'` (or whatever) to set the user role prior to calling `$this->can_read()`  
  
- `public function testCanReadData()`   
  
