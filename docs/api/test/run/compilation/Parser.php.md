<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/compilation/Parser.php  
  
# class Phad\Test\Compilation\Parser  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function run_test_exec(string $code, array $args, string $expect)`   
- `public function run_parse_test(string $view, string $target_html, array $var_exports, array $target_item_data)` Test a view with a single item. I don't even know how multiple items works yet.  
- `public function compare_data(array $actual, array $target_data, array $target_data_var_exports)`   
- `public function testPropertyFilter()`   
- `public function testAccessAttributeBeforeItem()`   
- `public function testAccessAttribute()`   
- `public function testForm()`   
- `public function testOn404_403()`   
- `public function testOn404()`   
- `public function testNestedItem()`   
- `public function testTwoItems()`   
- `public function testInnerLoop()`   
- `public function testDataItemClean()`   
- `public function testDataItem()`   
- `public function testSimpleItem()`   
  
