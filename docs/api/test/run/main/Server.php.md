<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/main/Server.php  
  
# class Phad\Test\Main\Server  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testUploadFileFail()`   
- `public function testFailedSubmissionRequiredProperties()`   
- `public function testFailedAccessForSubmission()`   
- `public function testCreateNoDelete()`   
- `public function testRejectDeleteAccessWithCustomHandler()`   
- `public function testFailToDeleteNonCandelete()`   
- `public function testDeleteCustomResponse()`   
- `public function testDeleteFailNotFound()`   
- `public function testDeletePrint()`   
- `public function testDeleteRedirect()`   
- `public function testDeleteResponse()`   
- `public function testDeleteItemS()`   
- `public function testCustomRowAccess()`   
- `public function testApproveCustomAccess()`   
- `public function testDenyCustomAccess()`   
- `public function testDenyRoleAccess()`   
- `public function testApproveRoleAccess()`   
- `public function testUploadFile()`   
- `public function testEditBlogForm()`   
- `public function testBlogFormRedirect()`   
- `public function testSubmitBlogForm()`   
- `public function testGetBlogForm()`   
- `public function testNestedViews()`   
- `public function testQueryCharacterJson()`   
- `public function testSimpleSpeciesQuery()`   
- `public function testQueryCharacterView()`   
- `public function testServeSitemap()`   
- `public function testGetDynamicRoute()`   
- `public function testRouted()`   
- `public function testExplicit()`   
- `public function testVerify()`   
  
