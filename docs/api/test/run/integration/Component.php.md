<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/Component.php  
  
# class Phad\Test\Integration\Component  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function prepare()`   
- `public function testCustomFilter()`   
- `public function testInlineModificationsWithNestedItems()`   
- `public function testQueryNestedItems()`   
- `public function testQueryAdjacentItems()`   
- `public function testNameFirstOfMultipleAccessNodesv2()`   
- `public function testNameSecondOfMultipleAccessNodesv2()`   
- `public function testNameFirstOfMultipleAccessNodes()`   
- `public function testNameSecondOfMultipleAccessNodes()`   
- `public function testQuerySecondOfMultipleAccessNodes()`   
- `public function testQueryingFirstOfMultipleAccesses()`   
- `public function testPassingObjectToIgnoreAccessNode()`   
- `public function testItemViewWithObject()`   
- `public function phad($idk=null)`   
- `public function item($name, $args=[])`   
  
