<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/Sitemap.php  
  
# class Phad\Test\Integration\Sitemap  
Tests sitemap creation  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testSitemapAttributesFromDeclaration()`   
- `public function testSitemapHandlerActive()`   
- `public function testSitemapHandlerDeclaredButNotSet()`   
- `public function testBuildSitemap()`   
- `public function testSitemapNodeData()`   
- `public function phad()`   
  
