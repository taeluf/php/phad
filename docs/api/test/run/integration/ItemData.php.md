<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/integration/ItemData.php  
  
# class Phad\Test\Integration\ItemData  
Tests for getting data from an item's view as an array  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testNestedObjects()`   
- `public function testAdjacentObjects()`   
- `public function testOneObject()`   
- `public function phad()`   
  
