<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
  
class Modal extends Autowire {  
  
    get form(){return this.q('.Form');}  
  
    show(){  
        this.n.classList.remove('hidden');  
    }  
    hide(){  
        this.n.classList.add('hidden');  
    }  
  
    setFormHtml(html){  
        this.form.innerHTML = html;  
        return this.q('form');  
    }  
  
    onclick(event){  
        if (event.target===this.n)this.hide();  
    }  
}  
  
Modal.aw();  
