<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Usage | Overview  
There are so many more featuers than what is listed here.  
  
## Sample View  
You can see many more example views in the tests at [test/Server/phad](/test/Server/phad) and [test/input/views](/test/input/views)  
```php  
<route pattern="/blog-list/"></route>  
  
<div item="Blog" >  
    <h1 prop="title"></h1>  
    <p prop="body"></p>  
</div>  
```  
  
## Sample Form  
You can see many more example forms in the tests at [test/Server/phad/form](/test/Server/phad/form) and [test/input/views/Form](/test/input/views/Form).  
  
```php  
<route pattern="/blog/make/"></route>  
  
  
<form cansubmit="call:can_submit" item="Blog" target="/blog/{slug}/">  
    <onsubmit><?php   
        $slug = strtolower($BlogRow['title']);  
        $slug = str_replace(' ', '-', $slug);  
        $BlogRow['slug'] = $slug;  
  
    ?></onsubmit>  
    <failsubmit><?php  
        // unset($BlogInfo->args['phad']);  
        // print_r($BlogInfo);  
        // print_r($_POST);  
        // // exit;  
        // print_r($BlogRow);  
        // // exit;  
    ?></failsubmit>  
  
    <input type="text" name="title" maxlength="75">  
    <textarea name="body" maxlength="2000" minlength="10"></textarea>  
  
  
    <input type="backend" name="slug" minlength=4 maxlength=150 />  
</form>  
```  
You'll need to add a submit button ...  
  
## Server Setup  
Example setup code with Liaison  
```php  
<?php  
  
  
$_GET['user'] = $_GET['user'] ?? 'default-user-role';  
  
$options = [  
    'item_dir'=>$dir.'/phad/',  
    'cache_dir'=>$dir.'/cache/',  
    'sitemap_dir'=>$dir.'/sitemap/',  
    'pdo' => $lildb->pdo(), // a pdo object  
    // 'user' => require(__DIR__.'/phad-user.php'), // a user object (no interface available ...)  
    'router' => $router,  
    'throw_on_query_failure'=>true,  
    'force_compile'=>false,  
];  
$phad = \Phad::main($options);  
  
$phad->filters['markdown'] = function($v){return 'pretend-this-is-markdown:<p>'.$v.'</p>';};  
  
$phad->integration->setup_liaison_routes($lia);  
$phad->integration->setup_liaison_route($lia, '/sitemap.xml', $phad->sitemap_dir.'/sitemap.xml');  
// $custom_access = require(__DIR__.'/phad-access.php'); // returns an Access object  
  
// $phad->access = $custom_access;  
  
$phad->access_handlers['main_msg'] =   
    function($ItemInfo){  
        echo "This is my custom deletion response. I don't care if deletion succeeded. Id was ".$ItemInfo->args['id'];  
        return false;  
    };  
  
$phad->access_handlers['never_allow'] =   
    function($ItemInfo){  
        return false;  
    }  
;  
  
$phad->access_handlers['can_submit'] =   
    function($ItemInfo, $ItemRow){  
        if (isset($_GET['deny_access'])&&$_GET['deny_access']=='true')return false;  
        return true;  
    }  
;  
  
$phad->access_handlers['permit_me'] =   
    function($data_node, $ItemInfo){  
        if ($_GET['permit_me']=='true')return true;  
        return false;  
    };  
  
$phad->handlers['user_has_role'] =   
function(string $role){  
    if (isset($_GET['user'])&&$role == $_GET['user'])return true;  
    return false;  
}  
;  
  
$phad->handlers['can_read_row'] =   
    function(array $ItemRow,object $ItemInfo,string $ItemName){  
        if (!isset($_GET['title']))return true;  
  
        if ($ItemRow['title'] == $_GET['title']){  
            return true;  
        }  
  
        return false;  
    };  
```  
  
## Upload Files  
This isn't integrated well, yet. You have to add some code to your form, like:  
```php  
<route pattern="/document/make/"></route>  
  
  
<form item="Document" target="/document-list/">  
    <onsubmit><?php  
  
        $DocumentRow['file_name'] = $_FILES['doc']['name'];  
        $DocumentRow['stored_name'] = \Phad\PDOSubmitter::uploadFile($_FILES['doc'],   
            dirname(__DIR__, 2).'/files-uploaded/',  
            ['txt']  
        );  
    ?></onsubmit>  
    <input type="text" name="title" maxlength="75" />  
    <input type="file" name="doc" />  
  
    <input type="backend" name="file_name" />  
    <input type="backend" name="stored_name" />  
</form>  
```  
  
### Simple Example  
```php  
<route pattern="/blog/{slug}/"></route>  
<div item="Blog" >  
    <p-data where="Blog.slug LIKE :slug"></p-data>  
    <h1 prop="title"></h1>  
    <x-prop prop="body" filter="commonmark:markdownToHtml"></x-prop>  
</div>  
```  
  
### File Uploads  
[test/Server/phad/form/document.php](/test/Server/phad/form/document.php)  
idk what to say. here's an example. Notice how i modify the document row & add the `type=backend` nodes  
```php  
<route pattern="/document/make/"></route>  
<form item="Document" target="/document-list/">  
    <onsubmit><?php  
        $DocumentRow['file_name'] = $_FILES['doc']['name'];  
        $DocumentRow['stored_name'] = \Phad\PDOSubmitter::uploadFile($_FILES['doc'],   
            dirname(__DIR__, 2).'/files-uploaded/',  
            ['txt']  
        );  
    ?></onsubmit>  
    <input type="text" name="title" maxlength="75" />  
    <input type="file" name="doc" />  
  
    <input type="backend" name="file_name" />  
    <input type="backend" name="stored_name" />  
</form>  
```  
