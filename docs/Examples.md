<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Examples  
Note (feb 4, 2022): These are old examples that i haven't properly reviewed and tested ... the examples are probably accurate, but ... idk  
  
  
## Write a view  
@ast_class(Phad\Test\Documentation,method.testWriteAView.docblock)  
```html  
<div item="Blog">  
    <h1 prop="title"></h1>  
    <p prop="description" filter="namespace:filter_name"></p>  
</div>  
```  
  
### Create a filter  
@ast_class(Phad\Test\Documentation,method.testCreateAFilter.docblock)  
```php  
# Import key "Filter.add" not found.  
```  
  
### Handle Errors  
@ast_class(Phad\Test\Documentation, method.testHandleErrors.docblock)  
```php  
<div item="Blog">  
    <on s=404><p>Not Found</p></on>  
    <on s=200><b>Type: <?=$Blog->type?></b></on>  
    <h1 prop="title"></h1>  
    <p prop="description"></p>  
</div>  
```  
  
### Inline Queries  
@ast_class(Phad\Test\Documentation, method.testInlineQuery.docblock)  
```php  
<div item="Blog">  
    <p-data where="Blog.type LIKE :type">  
        <on s=404><p><?=$type?> blogs not found</p></on>  
    </p-data>  
    <p-data where="Blog.type NOT LIKE :type" orderby="title ASC" limit="0,5">  
        <on s=200><p>non-<?=$type?> blogs</p></on>  
    </p-data>  
    <h1 prop="title"></h1>  
    <p prop="description"></p>  
</div>  
```  
  
### Routing  
@ast_class(Phad\Test\Documentation, method.testRouting.docblock)  
```html  
<route pattern="/blog-page/{slug}/"></route>  
<div item="Blog">  
    <p-data where="Blog.slug LIKE :slug"></p-data>  
    <h1 prop="title"></h1>  
    <p prop="description"></p>  
</div>  
```  
  
### Sitemap Generation  
@ast_class(Phad\Test\Documentation, method.testSitemapGeneration.docblock)  
```html  
<route pattern="/blog/{slug}/">   
    <sitemap   
        sql="SELECT slug, last_mod AS sm_lastmod FROM blog"   
        handler="namespace:blogSitemapHandler"  
        priority=0.8   
        changefreq=weekly  
    >  
    </sitemap>  
</route>  
  
<div item="Blog">  
    <p-data where="Blog.slug LIKE :slug"></p-data>  
    <h1 prop="title"></h1>  
    <p prop="description"></p>  
</div>  
```  
  
#### Defining the Sitemap Handler  
```php  
# Import key "Sitemap.addHandler" not found.  
```  
  
### Control Access  
```php  
This page only for viewing draft blog posts  
<div item="Blog">  
    <p-data name="draft"   
        where="Blog.slug LIKE :slug AND Blog.status LIKE 'draft'" limit="0,5"   
        access="role:admin|moderator;call:CanViewDraftPost;"  
    ></p-data>  
    <h1 prop="title"></h1>  
    <p prop="description"></p>  
</div>  
```  
  
#### Specify an access node to use  
```php  
# Import key "Access.SpecifyName" not found.  
```  
  
## Write a form  
```php  
<form item="Blog" target="/blog/{id}/">  
    This redirects to target after successful submission. Anyone can submit this form.  
    <input type="text" name="title" maxlength="75">  
    <textarea name="body" maxlength="2000" minlength="50"></textarea>  
</form>  
```  
  
### OnSubmit handler  
```php  
<form item="Blog" target="/blog/{slug}/">  
    Since `slug` is not (& shouldn't be) part of the form, we have to compute it on submit & use that for our redirection target.  
    <onsubmit><?php  
        $BlogRow['slug'] = str_replace(' ', '-', strtolower($BlogRow['title']));  
        /** set phad_mode to anything other than `submit` to skip submission (form will be displayed). */  
        if (false)$BlogItem->mode = 'cancel_submit';  
    ?>  
        On submission failure, the form will still be displayed. And any non-php inside the `onsubmit` tag will also be displayed (above the form tag). Though the onsubmit tag itself will be removed.  
    </onsubmit>  
    <input type="text" name="title" maxlength="75">  
    <textarea name="body" maxlength="2000" minlength="50"></textarea>  
  
  
    This `backend` node will be removed. But its required, so Phad knows to accept the `slug` attribute  
    <input type="backend" name="slug" minlength=4 maxlength=150 />  
</form>  
```  
  
## Spam Controls  
Add this to your form. You can change the string `'contact'` to identify different forms and you can pass an array other than `$_POST` as the submitted data.  
```php  
    <onsubmit><?php  
        $phad->verify_spam_control('contact', $_POST);  
    ?></onsubmit>  
    <?=$phad->show_spam_control('contact')?>  
```  
  
  
