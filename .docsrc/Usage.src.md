# Usage | Overview
There are so many more featuers than what is listed here.

## Sample View
You can see many more example views in the tests at @see_file(test/Server/phad) and @see_file(test/input/views)
```php
@file(test/Server/phad/blog2.php)
```

## Sample Form
You can see many more example forms in the tests at @see_file(test/Server/phad/form) and @see_file(test/input/views/Form).

```php
@file(test/Server/phad/form/blog.php)
```
You'll need to add a submit button ...

## Server Setup
Example setup code with Liaison
```php
@file(test/Server/phad-setup.php)
```

## Upload Files
This isn't integrated well, yet. You have to add some code to your form, like:
```php
@file(test/Server/phad/form/document.php)
```

### Simple Example
```php
<route pattern="/blog/{slug}/"></route>
<div item="Blog" >
    <p-data where="Blog.slug LIKE :slug"></p-data>
    <h1 prop="title"></h1>
    <x-prop prop="body" filter="commonmark:markdownToHtml"></x-prop>
</div>
```

### File Uploads
@see_file(test/Server/phad/form/document.php)
idk what to say. here's an example. Notice how i modify the document row & add the `type=backend` nodes
```php
<route pattern="/document/make/"></route>
<form item="Document" target="/document-list/">
    <onsubmit><?php
        $DocumentRow['file_name'] = $_FILES['doc']['name'];
        $DocumentRow['stored_name'] = \Phad\PDOSubmitter::uploadFile($_FILES['doc'], 
            dirname(__DIR__, 2).'/files-uploaded/',
            ['txt']
        );
    ?></onsubmit>
    <input type="text" name="title" maxlength="75" />
    <input type="file" name="doc" />

    <input type="backend" name="file_name" />
    <input type="backend" name="stored_name" />
</form>
```
