# Phad Architecture
I might fill this doc out ... idk. Don't hold your breath unless you're underwater or something. 

## Classes
This obviously is not complete ...

- `BlackHole`: placeholder object for displaying a view without an object (so all props & methods return empty strings ... prevents errors)
- `PDOSubmitter`: literally just saves submission to database & handles file upload. does NOT do any verification
- `SitemapBuilder`: a very simple utility class for writing sitemap xml to disk

### Compiler/Templating Features
this is for the built in `TemplateCompiler` and the templates in `code/template/`
- `@$$Placeholder` for literal replacement anywhere in the file
- `/* @@Placeholder */` for literal replacement anywhere in the file
- `/* @@start.Placeholder */` ... code ... `/* @end.Placeholder */` for blocks of code that are optional in the ouptut (requires `[Placeholder=>true]` to keep it)
- `/* @@if1.start.Placeholder */` and `@else1` is similar to `@start.Placeholder`, EXCEPT it auto adds `else` as needed in the chain of ifs. Multiple `@if1.start.NAME` can be chained. `@else1` is part of the same chain.
    - `@@if1....` is "smart" and will turn `if ()` into `elseif()`
    - `/* @@else1 */` is "stupid" and will literally be replaced with `else`

#### Unintended Features
- Replace any text in a template file by adding `$tempate_args['some_key'] = "Any text whatsoever"` and the `Any text whatsoever` will be replaced by `$data['some_key']` 


#### How compilation works (jan 24, 2022)
A breakdown of how we get from template + view to compiled output.

- there is a template for compiled output
- there is a view for compilation
- the template defines placeholders like `@$$PlaceholderName` and `/* PlaceholderName */`
- the view declares things like `item="Blog"`, `prop="title"`, and `<p-data sql="...">`
- Parser accepts the view (not the template)
- Parser removes phad attributes & phad nodes like `prop="title"` and `<p-data>` & inserts php code 
- some of the php code inserted is template code (containing `@$$Placeholder`s in each style)
- The parser converts the view into an array data structure containing the main code (where `prop="title"` is replaced by `<?=$Blog->title?>`), and a whole host of other key=>value pairs. 
- Each key in the array has a placeholder in the main template file.
- the key=>value array contains values like the code for handling different response codes
- the key=>value array also contains boolean values to indicate whether certain blocks of code should be output on compilation or not
- the Compiler processes the template and gets a list of template params
- the template params come in the form `placeholder_key => "literal representation"` like `@$$ItemInfo` becomes `['ItemInfo'=>@$$ItemInfo]`
- then the compiler takes in the data the Parser made from the view and the template params ... 
- for each template params, the compiler checks for the key/value pair in the Parser's view data ... 
- if the key/value pair is set, then `@$$ItemInfo` gets replaced with `$view_data['ItemInfo']`
- if the key/value pair is set for booleans, then `/* @@start.some_block */` ... some code ... `/* @@end.some_block */` gets output (if value true) or hidden (if value false)
- when using `@@if....`, calling `Compiler->doIfs($template, $data)` ... both `$template` and `$data` are by-reference and will be modified to make the `@@if` features work ... see the `Ifs` test. (`grep -R "@testIfs"`)
    - `@@__ifs1_#` is added to `$template`, immediately preceding the actual `if()` statement. `#` refers to the index in the chain of `@@if1` statements ...
    - `__ifs1_#` are added to `$data` with values either being empty string or `else`.
    - for `/* @@if1.start.abc */`, if `$data['abc'] == true` (prior to `doIfs()`), then `$data[__ifs1_#]` will be `else` (where `#` is the index of `if1.start.abc` amongst the `if1`s) ... otherwise `$data[__ifs1_#]` will be empty string.
    - `/* @@if1.start.abc */` gets replaced by `/* @@start.abc */` so the pre-existing optional-block feature works without changes

## Data Stack (getting item data as rows instead of html)
these notes are from when the data stack was implemented ... it's not any more ... but i might re-implement it later.

There are hooks in the view compilation for item/row start/finished. These hooks are used to build an array of nested data using a `stack/head` paradigm to properly fill the data array with the right nesting.  
