# Apps 
These are apps for Phad. They're not part of Phad's core & can be used as you please.

To enable an app:
```php
$lia->load_apps("vendor/taeluf/phad/apps/APP_DIR");
```

You may have to configure it. See `apps/APP_DIR/README.md` for additional dependencies and required configurations
