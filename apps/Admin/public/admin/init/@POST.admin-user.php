<?php

if (!$phad->initAccess->are_init_pages_enabled($lia)){
    echo "This page is disabled.";
    return;
}

// var_dump(get_class($package->phad));
// exit;



// print_r($_POST);
// exit;

$lib = new \Tlf\User\Lib($lia->pdo);
$user = $lib->user_from_email($_POST['email']);
if (!$user->is_registered()){
    $user->register($_POST['password']);
} else {
    $user->new_password($_POST['password'], $user->new_code("password_reset")); // \Tlf\User\Code::PASSWORD_RESET
}

if (!$user->is_active()){
    $code = $user->new_code('registration');
    $user->activate($code);
}

if (!$user->has_role('admin')){
    $user->add_role('admin');
}

echo "I think the user is setup with email ".$_POST['email'];

echo "Now login at <a href='/user/'>User Login</a>";
