<?php

if (!$phad->initAccess->are_init_pages_enabled($lia)){
    echo "This page is disabled.";
    return;
}

$file = dirname(__DIR__,4).'/Main/config.json';
if (file_exists($file)){
    $content = file_get_contents($file);
    $data = json_decode($content, true);
    if (!is_array($data))$data = [];
} else $data = [];

$post = $_POST;

$data['user.web_address'] = $post['web_address'];
$data['user.email_from'] = $post['email_from'];

file_put_contents($file, json_encode($data, JSON_PRETTY_PRINT));

echo "\n<br>";
echo file_get_contents($file);

$lib = new \Tlf\User\Lib($lia->pdo);  
  
// uncomment this line, run once, then re-comment this line  
$lib->init_db();
