<?php
$user = $lia->user;

if (!$phad->userAccess->is_admin($lia)||!$phad->userAccess->is_user_manager($lia)){
    echo "nope";
    return;
}
?>
<h1>User Management</h1>
<hr>
<h2>Create Role</h2>
<form action="roles/" method="POST">
    <label>New Role<br>
        <input type="text" name="role" required>
    </label><br>
    <input type="submit" value="Create Role" />

    <input type="hidden" name="which" value="new_role">
</form>
<hr>
<h2>Add Role to User</h2>
<form action="roles/" method="POST">
    <label>User Email<br>
        <select name="user_email" required>
            <option disabled selected>Select One</option>
            <?php
                $sql = "SELECT `email` from `user` ORDER BY `email`";
                $results = (new \Tlf\LilDb($lia->pdo))->query($sql);
                foreach ($results as $row){
                    $email = $row['email'];
                    echo "<option value=\"$email\">$email</option><br>";
                }
            ?>
        </select><br>
        
        <select name="role" required>
            <option disabled selected>Select One</option>
            <?php
                $sql = "SELECT DISTINCT `role` from `user_role` ORDER BY `role`";
                $results = (new \Tlf\LilDb($lia->pdo))->query($sql);
                foreach ($results as $row){
                    $email = $row['role'];
                    echo "<option value=\"$email\">$email</option><br>";
                }
            ?>
        </select><br>
    </label>

    <br>
    <input type="submit" value="Add Role to User" />


    <input type="hidden" name="which" value="add_role_to_user">
</form>

<hr>
<h2>Remove Role from user</h2>
<form action="roles/" method="POST">
    <label>User Email<br>
        <select name="user_email" required>
            <option disabled selected>Select One</option>
            <?php
                $sql = "SELECT `email` from `user` ORDER BY `email`";
                $results = (new \Tlf\LilDb($lia->pdo))->query($sql);
                foreach ($results as $row){
                    $email = $row['email'];
                    echo "<option value=\"$email\">$email</option><br>";
                }
            ?>
        </select><br>
        
    <br>
    <input type="submit" value="View & Remove Roles" />


    <input type="hidden" name="which" value="view_roles_of_user">
</form>
