<?php

namespace Phad\App\Admin;

class RoleInitAccess implements InitAccess {

    public function are_init_pages_enabled($lia): bool{
        return $this->is_it_manager($lia);
    }

    public function is_it_manager($lia): bool{
        if (isset($lia->props['user'])
            &&$lia->user->has_role('it_manager')
        ){
            return true;
        }

        return false;
    }

}
