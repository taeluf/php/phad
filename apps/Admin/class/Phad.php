<?php

namespace Phad\App\Admin;

class Phad extends \Phad {

    /**
     * Absolute path to directory to upload files into
     */
    public string $dir_upload;

    public \Phad\App\Admin\UserAccess $userAccess;
    public \Phad\App\Admin\InitAccess $initAccess;

    public function setInitAccess(\Phad\App\Admin\InitAccess $initAccess){
        $this->initAccess = $initAccess;
        // $this->access_handlers['admin.are_init_pages_enabled'] = [$access, 'are_init_pages_enabled'];
    }
    public function setUserAccess(\Phad\App\Admin\UserAccess $userAccess){
        $this->userAccess = $userAccess;
        // $this->access_handlers['admin.are_init_pages_enabled'] = [$access, 'are_init_pages_enabled'];
    }

    public function object_from_row($row, $ItemInfo){
        $orm_obj = $this->orm_obj($row,$ItemInfo->name);
        if ($orm_obj!=false)return $orm_obj;
        return parent::object_from_row($row, $ItemInfo);
    }

    public function orm_obj(array $row,string $name){
        $class = 'Dv\\File\\Db\\'.ucfirst($name);
        if (class_exists($class, true)){
            return new $class($row);
        }

        return false;
    }
}
