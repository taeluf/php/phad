<?php

namespace Phad\App\Admin;

interface InitAccess {

    /**
     * 
     */
    public function are_init_pages_enabled(\Lia $lia): bool;
    /**
     *
     */
    // public function

}
