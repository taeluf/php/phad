<?php

namespace Phad\App\Admin;

interface UserAccess {

    /**
     * 
     */
    public function is_admin(\Lia $lia): bool;
    public function is_user_manager($lia): bool;
    public function is_it_manager($lia): bool;
}
