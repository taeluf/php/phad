

/**
 * @param input the text input used for search
 */
async function conduct_photo_search(input){
    const url = '/files/search/?q=';

    // query for photos matching the search input string
    const r = new Request(url+input.value);
    const results = await fetch(r);
    const json = await results.json();
    const result_node = input.form.querySelector('.PhotoSearchResults');
    result_node.innerHTML = null;
    if (json.length==0)return;

    // add <img> nodes to the form
    for (const row of json){
        const img = document.createElement('img');
        img.src = row.url;
        img.setAttribute('data-id', row.id)
        result_node.insertBefore(img,null);
    }
}

/**
 * When a photo is clicked, update the `<input>` nodes in the form with the selected_id & selected_url, then submit the form.
 * listener is set on parent `<div>` 
 */
function photo_clicked(event, div){
    const img = event.target;
    if (img.tagName!='IMG')return;

    div.parentNode.querySelector('input[name="selected_id"]').value = img.getAttribute('data-id');
    div.parentNode.querySelector('input[name="selected_url"]').value = img.src;

    // submit form through `.click()` so `onsubmit()` handlers are called
    div.parentNode.querySelector('input[type="submit"]').click();
}
