<?php
/**
 *
 * For downloading via lookup_key
 * only allow public files
 *
 * MUST pass $_GET['key']
 */


$orig_dir = realpath($package->phad->dir_upload).'/';

$pdo = $lia->pdo;
$stmt = $pdo->prepare("SELECT * FROM `file` WHERE `lookup_key` LIKE :lookup_key AND is_public = 1");
$stmt->execute(['lookup_key'=>$_GET['key']]);

$rows = $stmt->fetchAll();
if (count($rows)!==1){
    echo "No file found at this url";
    return;
}


// only allow downloads of public files via lookup key
if ($rows[0]['is_public']!==1){
    echo "Not allowed to view file.";
    return;
}


// determine the download name
$dn_name = $rows[0]['download_name'] ?? $rows[0]['stored_name'];
$ext = pathinfo($dn_name, PATHINFO_EXTENSION);
if ($ext!=$rows[0]['file_type'])$dn_name .= $rows[0]['file_type'];


$real_path = $orig_dir.$rows[0]['stored_name'];
// send it
\Lia\FastFileRouter::set_download_file_name($dn_name);
\Lia\FastFileRouter::send_file($real_path);

exit;
