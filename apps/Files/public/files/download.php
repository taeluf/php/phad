<?php
/**
 * for downloading via stored_name
 */

$phad = $package->phad;

// verify the path is valid
// $orig_dir = realpath(dirname(__DIR__,2).'/uploaded-files/').'/';
$orig_dir = realpath($package->phad->dir_upload).'/';
// var_dump($orig_dir);
// exit;

$dirty_path = $orig_dir.$_GET['stored_name'];
$real_path = realpath($dirty_path);
$real_dir = dirname($real_path).'/';
if (strlen($orig_dir)<17
    ||
    $real_dir!==$orig_dir
){
    echo "File name error.";
    // var_dump($real_dir);
    // var_dump($orig_dir);
    return;
}

// get the file info frmo db
$pdo = $lia->pdo;
if ($phad->dataAccess->can_view_private_files()){
    $stmt = $pdo->prepare("SELECT * FROM `file` WHERE `stored_name` LIKE :stored_name");
} else {
    $stmt = $pdo->prepare("SELECT * FROM `file` WHERE `stored_name` LIKE :stored_name AND is_public = 1");
}
$stmt->execute(['stored_name'=>$_GET['stored_name']]);

$rows = $stmt->fetchAll();
if (count($rows)!==1){
    echo "No file found at this url";
    return;
}



if (!$phad->can_download_file($rows[0])){
  echo "Not allowed to view file.";
  return;
}


// determine the download name
$dn_name = $rows[0]['download_name'] ?? $rows[0]['stored_name'];
$ext = pathinfo($dn_name, PATHINFO_EXTENSION);
if ($ext!=$rows[0]['file_type'])$dn_name .= '.'.$rows[0]['file_type'];


// send it
\Lia\FastFileRouter::set_download_file_name($dn_name);
\Lia\FastFileRouter::send_file($real_path);

exit;
