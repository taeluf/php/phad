<?php
/**
 * You must show the modal on the page, outside of the form byc alling `$lia->view('story/modal');`
 * You must add a backend node with `name="photo_id"`
 *
 *
 * @param $url url for the existing photo/file thumb if exists
 * @param $id id for the existing photo/file thumb if exists
 *
 * @param $label (optional) the <form> or `Photo` by default
 * @param $column (optional) the column in the database
 */

$lia->addResourceFile(\Tlf\Js\Autowire::filePath());
$lia->setResourceOrder("js",["Autowire.js"],true);

$url = $url ?? $photo_url ?? '';
$id = $id ?? $photo_id ?? '';

$label = $label ?? 'Photo';
$column = $column ?? 'photo_id';
?>

<label><?=$label?><br><br>
    <noscript>Javscript is required to choose a <?=strtolower($label)?>.</noscript>
    <img class="File_Selector" src="<?=$url??'#'?>" alt="Select <?=$label?>" style="max-width:200px;cursor:pointer;padding:8px; border:1px solid rgba(0,0,0,0.3);" />
<br>
<br>
    <input type="hidden" name="<?=$column?>" value="<?=$id??''?>">
</label>
