# Files GUI
This is a Liaison + Phad Files GUI. 

## Setup
Setup Lia\Simple, of `taeluf/liaison` and setup the files app.
`deliver.php`:
```php
<?php

    require(__DIR__.'/vendor/autoload.php');

    // load configs
    R()->load(__DIR__.'/site/config.json');

    // basic server setup with Liaison
    $dir = __DIR__.'/code';

    $lia = new \Lia\Simple();
    $lia->debug = !$lia->is_production();


    // add the files app & set it up
    $Files_dir = __DIR__.'/vendor/taeluf/phad/apps/Files/';
    $lia->deliver_files("$Files_dir/public-files/");
    $lia->load_apps("$Files_dir");

    // add the autowire js file
    // You may want to do this conditionally ... later it will be handled by the Files app itself
    $lia->addResourceFile(\Tlf\Js\Autowire::filePath());
```

And configure it. 

`site/config.json`:
```json
    {
        "files.upload_dir": "file-uploads/"
    }
```

## Dependencies
- Install `taeluf/liaison v0.6.x-dev`
- Install `taeluf/phad v0.4.x-dev`
- Install `"taeluf/js.autowire": "v0.1.x-dev"`. Add JS dependency in php `$lia->addResourceFile(\Tlf\Js\Autowire::filePath());`
- Install `taeluf/resource v0.2.x-dev`. Load Configs with `R()->load(__DIR__.'/site/config.json')`;

## Configs
- `"files.upload_dir": "backup/file-uploads/"`
- `files.class.data_access` optional class name implementing `Tlf\Phad\Files\DataAccess` 

## Setup Database
Execute `up.sql` file(s) in `Files/db/v1/`. Do so manually or use lildb:
```
composer install vendor/lildb`
# pdo.php must return a PDO instance
vendor/bin/ldb migrate 0 1 -dir vendor/taeluf/phad/apps/Files/db/ -db path/to/pdo.php
```
