<route pattern="/files/"></route>

<a href="/files/upload/"><button>Upload File</button></a>
<h1>Files</h1>
<section class="Files">
    <div item="file">
        <p-data data_loader="dv:get_files" access="call:dv.can_view_files"></p-data>
        <a class="img" href="<?=$file->editUrl?>">
            <img src="<?=$file->thumb_url?>" alt="<?=$file->alt_text?>" />
        </a>
        <span>
            <a href="<?=$file->downloadUrl?>"><small prop="download_name"></small></a>
            <?php if ($file->lookup_key!=null&&$file->lookup_key!=''):  ?>
                <br>
                <a if="" href="<?=$file->url?>">@<small prop="lookup_key"></small></a>
            <?php endif;?>
        </span>
    </div>
</section>
