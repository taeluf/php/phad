<?php

namespace Dv\File\Db;

class File extends \Tlf\LilOrm {


    public function getPath(){
      return dirname(__DIR__,2).'/uploaded-files/'.$this->stored_name;
    }

    public function getDeleteUrl(){
        return '/files/upload/?phad_action=delete&id='.$this->id;
    }

    public function getEditUrl(){
        return '/files/upload/?id='.$this->id;
    }

    public function getDownloadUrl(){
        return '/files/download/?stored_name='.$this->stored_name;
    }

    public function getThumb_url(){
        $type = strtolower($this->file_type);
        $image_types = ['png','jpg','jpeg','gif','webp'];
        if (in_array($type,$image_types)){
            return $this->getUrl();
        }

        if (file_exists(dirname(__DIR__,2).'/public-files/files/'.$type.'.jpeg')){
            return '/files/'.$type.'.jpeg';
        }

        if (!isset($this->id))return '#';
        return '/files/placeholder.jpeg';
    }
    public function getUrl(){
        if ($this->lookup_key!=null&&$this->lookup_key!=''){
            return '/files/view/?key='.$this->lookup_key;
        }
        return '/files/download/?stored_name='.$this->stored_name;
    }
}

